import {Knex} from "knex";

export class ChatRoomService{
    constructor(private knex:Knex){}

    async postMessage(userId:number|undefined,receiverId:number,message:string){
        if(userId){
            return await this.knex.insert({sent_user:userId,received_user:receiverId,content:message}).into("chat").returning("created_at");
        }
		return null
    }

	async getMessage(userId:number|undefined,receiverID:number){
		if(userId){
			return await this.knex.select("sent_user","content","created_at").from("chat").where({sent_user:userId,received_user:receiverID}).orWhere({sent_user:receiverID,received_user:userId}).orderBy("created_at");
		}
		return null
	}
}