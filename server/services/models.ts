
export interface User{
    id:number
    username:string
    password:string
}

export interface QuestBoard{
    id: number;
    stage: string;
    quest_name: string;
    user_id: number;
    publish_date: string;
    duration: string;
    expiration_date: string;
    prize: number;
    bail: number;
    difficulty: number;
    photo: string;
    about_quest: string;
    completed_by_user: number;
    took_by_user: number;
}

export interface PublishQuestDetails{
	stage:string;
	quest_name:string;
	publish_date:string;
	duration:string;
	expiration_date:string;
	prize:number;
	bail:number;
	difficulty:number;
	photo:string;
	about_quest:string;
}

export interface currentUsers{
    id:number;
    username:string;
    password:string;
    email:string;
    currency:number;
    rank:number|string;
    taking_quest:number;
    created_at:string;
    photo:string;
    achievemnt:string;
    display_name:string;
    self_inro:string;
}

declare global{
    namespace Express{
        interface Request{
            user?: User
        }
    }
}