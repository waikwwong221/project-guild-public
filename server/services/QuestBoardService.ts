import {Knex} from "knex";
import {PublishQuestDetails} from "./models";

export class QuestBoardService{
	constructor(private knex:Knex){}

	async questBoard() {
        return await this.knex
            .select("*","users.id as user_id","quests.id as quest_id", "quests.photo as quest_photo", "users.photo as users_photo")
            .from("quests")
			.where('stage', 'like', 'published')
            .innerJoin("users","quests.user_id","users.id")
    }

	async publishQuest(quest_name:string,duration:string,expiration_date:Date,userId:number,prize:number,bail:number,difficulty:number,photo:string,
	about_quest:string){
		
		await this.knex.insert({stage:"published",quest_name,publish_date:new Date(),duration,expiration_date,user_id:userId,prize,bail,difficulty,photo,
		about_quest}).into("quests")

		await this.knex("users")
	   .decrement("currency", prize)
	   .where("id", userId)
	}

	async selectQuest(userId:number|undefined,id:number|string) {
       	// let questDetails:PublishQuestDetails[] = await this.knex.select("*").from("quests").where("id",id);
		if(userId){
			await this.knex.insert({user_id:userId,quest_id:id}).into("application")
			await this.knex("quests")
			.update({stage:"accepted",took_by_user:userId, updated_at:new Date})
			.where("id",id)

			let took_by_user = (await this.knex.select("took_by_user").from("quests").where("id",id))[0].took_by_user;
			let quest_bail = (await this.knex.select("bail").from("quests").where("id",id))[0].bail; 
			await this.knex("users")
	   		.decrement("currency", quest_bail)
	   		.where("id", took_by_user)

		}
    }

	async publishedQuest() {
        return await this.knex
        .select("*","users.id as user_id","quests.id as quest_id", "quests.photo as quest_photo", "users.photo as users_photo")
        .from("quests")
        .innerJoin("users","quests.user_id","users.id")
    }

	async acceptedQuest() {
		return await this.knex
		.select("*","quests.id as quest_id")
		.from('quests')
		.where('stage', 'like', 'accepted' )
		.orWhere('stage', 'like' ,'finished') 
	}

	
	async reportFinishedQuest(id:number) {
		return await this.knex("quests")
		.update({stage:"finished", updated_at:new Date()})
		.where("id",id)
	}

	async reportCompletedQuest(id:number) {
		let took_by_user = (await this.knex.select("took_by_user").from("quests").where("id",id))[0].took_by_user;
		let quest_rewards = (await this.knex.select("prize").from("quests").where("id",id))[0].prize;
		let quest_bail = (await this.knex.select("bail").from("quests").where("id",id))[0].bail;
		let published_by_user = (await this.knex.select("user_id").from("quests").where("id",id))[0].user_id;
		await this.knex("quests")
	   .update({stage:"completed", completed_by_user:took_by_user, updated_at:new Date(), })
	   .where("id",id)

	   await this.knex("users")
	   .increment("currency", quest_rewards)
	   .where("id", took_by_user)
	   await this.knex("users")
	   .increment("currency", quest_bail)
	   .where("id", took_by_user)
	   await this.knex("users")
	   .increment("rank", 1)
	   .where("id", took_by_user)

	//    await this.knex("users")
	//    .decrement("currency", quest_rewards)
	//    .where("id", published_by_user)
	   await this.knex("users")
	   .decrement("currency", quest_bail)
	   .where("id", published_by_user)
	}


	async abandonQuest(id:number) {
		return await this.knex("quests")
		.update({stage:"published", updated_at:new Date()})
		.where("id",id)

	}

	async cancelQuest(id:number) {
		await this.knex("quests")
		.update({stage:"cancelled", updated_at:new Date()})
		.where("id",id)

		let took_by_user = (await this.knex.select("took_by_user").from("quests").where("id",id))[0].took_by_user;
		let quest_rewards = (await this.knex.select("prize").from("quests").where("id",id))[0].prize;
		let quest_bail = (await this.knex.select("bail").from("quests").where("id",id))[0].bail;
		let published_by_user = (await this.knex.select("user_id").from("quests").where("id",id))[0].user_id;
		await this.knex("users")
	   .increment("currency", quest_bail)
	   .where("id", took_by_user )


	   await this.knex("users")
	   .increment("currency", quest_rewards)
	   .decrement("currency", quest_bail)
	   .where("id", published_by_user)
	}


// 	async reportFinishedQuest(id:number) {
// 		await this.knex("quests")
// 	   .update({stage:"finished", updated_at:new Date(),completed_by_user:userId})
// 	   .where("id",id)
//    }


}