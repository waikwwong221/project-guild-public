import {Knex} from "knex";
import {uuidGenerator} from "../main";

export class UserService{
	constructor(private knex:Knex){}

	async login(username:string){
		return await this.knex.select("*").from("users").where("username",username);
	}

	async loginByEmail(email:string){
		return await this.knex.select("*").from("users").where("email",email);
	}

	async currentUsers(){
		return await this.knex.select("*").from("users");
	}

	async loadUserData(){
		return await this.knex.select("*").from("users");
	}

	async userDataUpdate(self_intro: string, photo: string,display_name:string, userId: number|undefined){
		// if(userId && photo){
		if(photo){
			// console.log("A")
			// console.log(self_intro)
			// console.log(photo);
			// console.log(display_name)
			// console.log(userId)
		await this.knex("users")
		.update({photo:photo, self_intro:self_intro, display_name:display_name})
		.where("id",userId)
		}else{
			await this.knex("users")
		.update({self_intro:self_intro, display_name:display_name})
		.where("id",userId)
		}
	}

	async createUser(username:string,password:string,email:string){
		// const uuid = await uuidGenerator();
		await this.knex.insert({username,password,email,currency:0,rank:0,created_at:new Date()}).into("users");
	}

	async resetPasswordByEmail(email:string){
		return await this.knex.select("*").from("users").where("email",email);
	}

	async createSocialUser(username:string,password:string,email:string):Promise<number>{
		// const uuid = await uuidGenerator();
		return await this.knex.insert({username,password,email,currency:0,rank:0,created_at:new Date()}).into("users").returning("id");
	}

	async createSocialUserWithoutEmail(username:string,password:string):Promise<number>{
		// const uuid = await uuidGenerator();
		return await this.knex.insert({username,password,currency:0,rank:0,created_at:new Date()}).into("users").returning("id");
	}

	async getUserById(id:number){
		return await this.knex.select("*").from("users").where("id",id);
	}

	async addUserMoney(id:number, coins:number){
		let currency = await this.knex.select("currency").from("users").where("id",id);
		await this.knex("users").update("currency",currency[0].currency + coins).where("id",id)
	}
}