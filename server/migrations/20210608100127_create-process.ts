import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    await knex.schema.createTable("process", (table) => {

        table.integer("user_id").notNullable();
        table.integer("quest_id").notNullable();
        table.string("stage");
        table.boolean("likes");
        table.timestamps(false,true);
    })
}



export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTable("process")
}

