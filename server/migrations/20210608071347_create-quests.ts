import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    await knex.schema.createTable("quests", (table) => {
        table.increments();
        table.string("stage");
        table.string("quest_name").notNullable;
        table.integer("user_id").notNullable;
        table.timestamp("publish_date");
        table.string("duration");
        table.date("expiration_date");
        table.float("prize");
        table.float("bail");
        table.integer("difficulty");
        table.string("photo");
        table.string("about_quest");
        table.integer("completed_by_user");
        table.integer("took_by_user")
        table.timestamp("updated_at");
    })
}



export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTable("quests")
}

