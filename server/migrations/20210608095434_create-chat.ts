import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    await knex.schema.createTable("chat", (table) => {

        table.integer("sent_user").notNullable();
        table.integer("received_user").notNullable();
        table.string("content");
        table.timestamps(false,true);
    })
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTable("chat")
}

