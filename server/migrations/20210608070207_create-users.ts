import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    await knex.schema.createTable("users", (table) => {
        table.increments();
        table.string("username").notNullable().unique();
        table.string("password").notNullable();
        table.string("email");
        table.integer("currency");
        table.integer("rank");
        table.integer("taking_quest");
        table.timestamp("created_at");
        table.string("achievement");
        table.string("display_name");
        table.string("photo");
        table.string("self_intro")



    })
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTable("users")
}

