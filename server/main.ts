import express,{Request,Response} from "express";
import Knex from "knex";
import dotenv from "dotenv";
import cors from "cors";
import {UserService} from "./services/UserService";
import {UserController} from "./controllers/UserController";
import {chatRoomRoutes, initRoutes,questBoardRoutes,userRoutes} from "./routes";
import {isLoggedIn} from "./guards";
import {v4 as uuid} from "uuid";
import { QuestBoardController } from "./controllers/QuestBoardControllers";
import { QuestBoardService } from "./services/QuestBoardService";
import multer from "multer";
import path from "path";
const knexfile = require("./knexfile");
const knex = Knex(knexfile[process.env.NODE_ENV || "development"]);
// import {Server as SocketIO} from "socket.io";
import http from "http";
import {Server as SocketIO,Socket} from "socket.io";
import {logger} from "./logger";
import { ChatRoomService } from "./services/ChatRoomService";
import { ChatRoomController } from "./controllers/ChatRoomController";
import { join } from "path/posix";

dotenv.config();

const app = express();
export const server = new http.Server(app);

// app.use(cors({
//     origin: 'http://pantherguild.art'
// }))

app.use(cors({
    origin: ["http://localhost:3000","https://pantherguild.art","https://guild.pantherguild.art"]
}))

app.use(express.urlencoded({extended:true}));
app.use(express.json());

// io.use((socket:Socket, next) => {
//     const request = socket.request as express.Request;
//     sessionMiddleware(request, request.res as express.Response, next as express.NextFunction);
// }

const storage = multer.diskStorage({
    destination(_req, _file, cb) {
        cb(null, path.resolve('./uploads'));
    },
    filename(_req, file, cb) {
        cb(null, `${file.fieldname}-${Date.now()}.${file.mimetype.split('/')[1]}`);
    },
})

export const upload = multer({storage,limits:{fileSize:3 * 1000 * 1000}})

export async function uuidGenerator():Promise<any>{
    let id = uuid();
    const uuids = await knex.select("uuid").from("users");
    if(uuids[0]){
        for(let individual of uuids){
            if(individual.uuid === id){
                return uuidGenerator();
            }else{
                return id;
            }
        }
    }else{
        return id;
    }
}

export const userService = new UserService(knex);
export const userController = new UserController(userService);
export const questBoardService = new QuestBoardService(knex);
export const questBoardController = new QuestBoardController(questBoardService);
export const chatRoomService = new ChatRoomService(knex);
export const chatRoomController = new ChatRoomController(chatRoomService);
initRoutes();

app.use("/users",userRoutes);
app.use(express.static("uploads"))
// app.use(express.static("../react/build"))
app.use(isLoggedIn);
app.use("/chatroom",chatRoomRoutes);
app.use("/",questBoardRoutes);
// app.use(express.static("uploads"))
export const io = new SocketIO(server,{
	cors:{
		origin:["http://localhost:3000","https://pantherguild.art","https://guild.pantherguild.art"]
	}
})

io.on("connection",(socket:Socket) => {
    try{
		// console.log((socket.request as express.Request).user)
        // if (!(socket.request as express.Request).user) {
		// 	console.log("a")
        //     socket.disconnect();
        // }
		socket.on("message",data=>{
			io.emit(`new-message-${data.sender}-${data.receiver}`,{message:data.message,send:true});
			io.emit(`new-message-${data.receiver}-${data.sender}`,{message:data.message,send:false});
		})
    }catch (e) {
        logger.error(e);
    }
})

const port = process.env.SERVER_PORT;
server.listen(port,function(){
	console.log(`Listening at http://localhost:${port}`)
})
