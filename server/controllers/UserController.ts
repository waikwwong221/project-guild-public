import {UserService} from "../services/UserService";
import {Request,Response} from "express";
import {checkPassword, hashPassword} from "../hash";
import jwtSimple from "jwt-simple";
import jwt from "../jwt";

import {currentUsers, User} from "../services/models";
import nodemailer from "nodemailer";
import fetch from "node-fetch";
import crypto from "crypto";
import { logger } from "../logger";

export class UserController{
	constructor(private userService:UserService){}

	login = async(req:Request,res:Response) => {
		try{
			const {username,password} = req.body;
			const user:User = (await this.userService.login(username))[0];
			// console.log(req.body)
			if(!user){
				// console.log("b")
				return res.status(400).json({message:"User does not exist"})
			}

			if(!(await checkPassword(password,user.password))){
				// console.log("a")
				return res.status(400).json({message:"Password is incorrect"})
			}

			const token = jwtSimple.encode({
				id:user.id,
				username,
				password
			},jwt.jwtSecret)

			return res.status(200).json({success:true,data:token,userId:user.id})
		}catch(e){
			logger.error(`path:${req.path} method:${req.method}`);
			logger.error(e);
			return res.status(500).json({success:false,message:e.toString()});
		}
	}

	currentUsers = async (req: Request,res: Response) => {
		try{
			// const currentUsers:User = (await this.userService.currentUsers())[0];
            res.json({success:true,id:req.user?.id});
			// const user:currentUsers[] = await this.userService.currentUsers();
			// return res.status(200).json({success:true,data:questBoard})
		}catch(e){
			// logger.error(`path:${req.path} method:${req.method}`);
			// logger.error(e);
			res.status(500).json({message: "internal server error"});
			// return res.status(500).json({success:false,message:e.toString()});
		}
	}

	loadUserData = async (req: Request,res: Response) => {
		try{
			const UserDate = await this.userService.loadUserData();
            res.json(UserDate);
		}catch(e){
			res.status(500).json({message: "internal server error"});
		}
	}

	register = async(req:Request,res:Response) => {
		try{
			// console.log(req.body)
			const {username,password,email} = req.body;

			let hashedPassword = await hashPassword(password);
			await this.userService.createUser(username,hashedPassword,email);
			return res.status(200).json({success:true})
		}catch(e){
			// logger.error(`path:${req.path} method:${req.method}`);
			// logger.error(e);
			return res.status(500).json({success:false,message:e.toString()});
		}
	}

	userDataUpdate = async (req: Request,res: Response) => {
		try{
			console.log(req.body)
			const {display_name,self_intro} = req.body;
			// console.log(req.user?.id)

			if(req.file){
				req.body.photo = req.file.filename;
			}

			await this.userService.userDataUpdate(self_intro,req.body.photo,display_name,req.user?.id);
			return res.status(200).json({success:true})
		}catch(e){
			logger.error(`path:${req.path} method:${req.method}`);
			logger.error(e);
			return res.status(500).json({success:false,message:e.toString()});
		}
	}

	resetPasswordByEmail = async(req:Request,res:Response) => {
		try{
			let user = (await this.userService.resetPasswordByEmail(req.body.email))[0];
			if(!user){
				return res.status(400).json({message:"This email is not registerd"})
			}
			const transporter = nodemailer.createTransport({
                host: "smtp.gmail.com",
                port: 587,
                secure: false,
                auth: {
                    user: process.env.GMAIL_USER,
                    pass: process.env.GMAIL_PASS,
                },
            });
            let info;
            info = await transporter.sendMail({
                from: "emojichattecky@gmail.com", // sender address
                to: user.email, // list of receivers
                subject: "重設密碼", // Subject line
                text: `Dear ${user.username},\nClick the following link to reset your password\n${process.env.FRONTEND_PORT}/resetPassword?uuid=${user.uuid}\n`,
            });
            // send mail with defined transport object
            // logger.debug(info);
			return res.status(200).json({success:true})
		}catch(e){
			// logger.error(`path:${req.path} method:${req.method}`);
			// logger.error(e);
			return res.status(500).json({success:false,message:e.toString()});
		}
	}

	googleLogin = async(req:Request,res:Response) => {
		try{
			const fetchRes = await fetch("https://www.googleapis.com/oauth2/v2/userinfo", {
                method:"get",
                headers:{
                    Authorization: `Bearer ${req.body.accessToken}`,
                },
            });
			const result = await fetchRes.json();
			// console.log(result)
			if(!result){
				return res.status(400).json({message:"User does not exist"})
			}
			let token;
            let user = (await this.userService.login(result.name))[0];
			let loginByEmail = (await this.userService.loginByEmail(result.email))[0];
            // logger.debug(user);
            if (!user && !loginByEmail) {
                user = {
                    username: result.name,
                    password: await hashPassword(crypto.randomBytes(30).toString()),
                };
                user.id = (await this.userService.createSocialUser(user.username,user.password,result.email))[0];
                req.user = user;
            }else if(user){
                req.user = user;
            }

			if(user){
				token = jwtSimple.encode({
					id:user.id,
					username:user.username,
					password:user.password
				},jwt.jwtSecret)
				return res.status(200).json({success:true,id:user.id,data:token});
			}else{
				token = jwtSimple.encode({
					id:loginByEmail.id,
					username:loginByEmail.username,
					password:loginByEmail.password
				},jwt.jwtSecret)
				return res.status(200).json({success:true,id:loginByEmail.id,data:token});
			}
        }catch(e){
            // logger.error(`path:${req.path} method:${req.method}`);
            // logger.error(e);
            return res.status(500).json({message:e.toString()});
        }
	}

	addUserMoney =  async (req: Request, res: Response) => {
        try {
        //    console.log(req.body)

            await this.userService.addUserMoney(parseInt(req.body.id), parseInt(req.body.coins));
            res.json({success:true});
        } catch (e) {
            logger.error(e);
            logger.error(`path:${req.path} method:${req.method}`)
            res.status(500).json({message: "can't add money to user"});
        }
    }

	facebookLogin = async(req:Request,res:Response) => {
		try{
			// console.log(req.body)
			const fetchRes = await fetch(`https://graph.facebook.com/me?access_token=${req.body.accessToken}`, {
                method:"get",
                headers:{
                    Authorization: `Bearer ${req.body.accessToken}`,
                },
            });
			const result = await fetchRes.json();
			// console.log(result)
			if(!result){
				res.status(400).json({success:false,message:"User does not exist"})
			}
			let token;
            let user = (await this.userService.login(result.name))[0];
			if(result.email){
				let loginByEmail = (await this.userService.loginByEmail(result.email))[0];
				// logger.debug(user);
				if (!user && !loginByEmail) {
					user = {
						username: result.name,
						password: await hashPassword(crypto.randomBytes(30).toString()),
					}
					user.id = (await this.userService.createSocialUser(user.username,user.password,result.email))[0];
					req.user = user;
				}else if(user){
					req.user = user;
				}else if(loginByEmail){
					req.user = user;
				}

				if(user){
					token = jwtSimple.encode({
						id:user.id,
						username:user.username,
						password:user.password
					},jwt.jwtSecret)
					res.status(200).json({success:true,id:user.id,data:token});
				}else{
					token = jwtSimple.encode({
						id:loginByEmail.id,
						username:loginByEmail.username,
						password:loginByEmail.password
					},jwt.jwtSecret)
					res.status(200).json({success:true,id:loginByEmail.id,data:token});
				}
			}else{
				if (!user) {
					user = {
						username: result.name,
						password: await hashPassword(crypto.randomBytes(30).toString()),
					}
					user.id = (await this.userService.createSocialUserWithoutEmail(user.username,user.password))[0];
					req.user = user;
				}else if(user){
					req.user = user;
				}

				token = jwtSimple.encode({
					id:user.id,
					username:user.username,
					password:user.password
				},jwt.jwtSecret)
				res.status(200).json({success:true,id:user.id,data:token});
			}
        }catch(e){
            logger.error(`path:${req.path} method:${req.method}`);
            logger.error(e);
            res.status(500).json({message:e.toString()});
        }
	}
}
