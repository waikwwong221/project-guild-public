import { QuestBoardService } from "../services/QuestBoardService";
import { Request, Response } from "express";
import { logger } from "../logger";
import { id } from "date-fns/locale";

export class QuestBoardController{
    constructor(private questBoardService:QuestBoardService){}

    questBoard = async (req: Request, res: Response) => {
        try {
            const questBoard = await this.questBoardService.questBoard();
            res.json(questBoard);
            // console.log("questBoard", questBoard)
        } catch (e) {
            logger.error(e)
            logger.error(`path:${req.path} method:${req.method}`)
            res.status(500).json({message: "internal server error"});
        }
    }

	publishQuest = async (req: Request, res: Response) => {
        try {
			let {quest_name,publish_date,duration,expiration_date,userId,prize,bail,difficulty,about_quest} = req.body;
            // req.body.publish_date = new Date();
            // req.body.status = "published"
			if(req.file){
				req.body.photo = req.file.filename;
			}
			// console.log(req.body);
            await this.questBoardService.publishQuest(quest_name,duration,expiration_date,parseInt(userId),prize,bail,
			difficulty,req.body.photo,about_quest);
            res.json({success:true});
        } catch (e) {
            res.status(500).json({message: "internal server error"});
        }
    }

    questBoardApplication =  async (req: Request, res: Response) => {
        try {
            // console.log("A")

            await this.questBoardService.selectQuest(req.user?.id,parseInt(req.params.number));
            // await this.questBoardService.publishQuest(quest_name,duration,expiration_date,parseInt(userId),prize,bail,
			// difficulty,req.body.photo,about_quest);
            console.log(req.user?.id)
            res.json({success:true});


        } catch (e) {
            res.status(500).json({message:e.toString()});
        }
    }

    publishedQuest = async (req: Request, res: Response) => {
        try {
            const publishedQuest = await this.questBoardService.publishedQuest();
            res.json(publishedQuest);
        } catch (e) {
            res.status(500).json({message: "internal server error"});
        }
    }

    acceptedQuest =  async (req: Request, res: Response) => {
        try {
            const acceptedQuest = await this.questBoardService.acceptedQuest();
            res.json(acceptedQuest);
        } catch (e) {
            res.status(500).json({message: "internal server error"});
            logger.error(e)
            logger.error(`path:${req.path} method:${req.method}`)
        }
    }

    reportFinishedQuest =  async (req: Request, res: Response) => {
        try {
            console.log("req.body.params.id", req.params.id)
            console.log("req.body.params.id", req.params.prize)

            await this.questBoardService.reportFinishedQuest(parseInt(req.params.id));
            res.json({success:true});
        } catch (e) {
            logger.error(e);
            logger.error(`path:${req.path} method:${req.method}`)
            res.status(500).json({message: "can't report finished quests"});
        }
    }

    reportCompletedQuest =  async (req: Request, res: Response) => {
        try {
            console.log("req.body.params.id", req.params.id)
            console.log("req.body.params.id", req.params.prize)

            await this.questBoardService.reportCompletedQuest(parseInt(req.params.id));
            res.json({success:true});
        } catch (e) {
            logger.error(e);
            logger.error(`path:${req.path} method:${req.method}`)
            res.status(500).json({message: "can't report finished quests"});
        }
    }

    abandonQuest =  async (req: Request, res: Response) => {
        try {
            await this.questBoardService.abandonQuest(parseInt(req.params.id));
            res.json({success:true});
        } catch (e) {
            logger.error(e);
            logger.error(`path:${req.path} method:${req.method}`)
            res.status(500).json({message: "can't abandon quests"});
        }
    }

    cancelQuest =  async (req: Request, res: Response) => {
        try {
            await this.questBoardService.cancelQuest(parseInt(req.params.id));
            res.json({success:true});
        } catch (e) {
            logger.error(e);
            logger.error(`path:${req.path} method:${req.method}`)
            res.status(500).json({message: "can't cancel quests"});
        }
    }

    // reportFinishedQuest =  async (req: Request, res: Response) => {
    //     try {
    //         console.log("req.body.params.id", req.params.id)

    //         await this.questBoardService.reportFinishedQuest(req.user?.id,parseInt(req.params.id));
    //         res.json({success:true});
    //     } catch (e) {
    //         logger.error(e);
    //         logger.error(`path:${req.path} method:${req.method}`)
    //         res.status(500).json({message: "can't report finished quests"});
    //     }
    // }
}