import express,{Request,Response} from "express";
import {logger } from "../logger";
import {ChatRoomService} from "../services/ChatRoomService";

export class ChatRoomController{
    constructor(private chatRoomService:ChatRoomService){}

    postMessage = async(req:Request,res:Response) => {
        try{
            // console.log(req.user)
            const time = await this.chatRoomService.postMessage(req.user?.id,req.body.receiverId,req.body.message)
			if(time){
				res.status(200).json({success:true,data:{content:req.body.message,created_at:time[0]}});
			}else{
				res.status(400).json({success:false});
			}

        }catch(e){
            logger.error(`path:${req.path} method:${req.method}`);
            logger.error(e);
            res.status(500).json({success:false,message:e.toString()});
        }
    }

	getMessage = async(req:Request,res:Response) => {
		try{
			// console.log(req.user?.id)
			const receiverId = req.params.id;
			const message = await this.chatRoomService.getMessage(req.user?.id,parseInt(receiverId))
			if(message){
				res.status(200).json({success:true,message})
			}else{
				res.status(400).json({success:false,message:"User does not exist"})
			}
		}catch(e){
			logger.error(`path:${req.path} method:${req.method}`)
			logger.error(e)
			res.status(500).json({success:false,message:e.toString()})
		}
	}
}