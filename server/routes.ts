import express from "express";
import {chatRoomController, questBoardController, upload, userController} from "./main";
import { isLoggedIn } from './guards';

export const userRoutes = express.Router();
export const questBoardRoutes = express.Router();
export const chatRoomRoutes = express.Router();
// export const applicationRoutes = express.Router();

export function initRoutes(){
	userRoutes.post("/login",userController.login);
	userRoutes.get("/currentUsers",isLoggedIn,userController.currentUsers);
	userRoutes.get("/loadUserData",userController.loadUserData);
	userRoutes.post("/register",upload.single(""),userController.register);
	userRoutes.post("/userDataUpdate",isLoggedIn,upload.single("photo"),userController.userDataUpdate);
	userRoutes.post("/passwordByEmail",userController.resetPasswordByEmail);
	userRoutes.post("/googleLogin",userController.googleLogin);
	userRoutes.post("/addUserMoney",upload.single(""),userController.addUserMoney);
	userRoutes.post("/facebookLogin",userController.facebookLogin);

	questBoardRoutes.get("/questBoard",questBoardController.questBoard);
	questBoardRoutes.post("/questBoard",upload.single("photo"),questBoardController.publishQuest);
	questBoardRoutes.get("/questBoardApplication/:number", questBoardController.questBoardApplication)
	questBoardRoutes.get("/frontPage", questBoardController.questBoardApplication)
	questBoardRoutes.get("/publishedQuest", questBoardController.publishedQuest)
	questBoardRoutes.get("/acceptedQuest", questBoardController.acceptedQuest)
	questBoardRoutes.get("/reportFinishedQuest/:id", questBoardController.reportFinishedQuest)
	questBoardRoutes.get("/reportCompletedQuest/:id", questBoardController.reportCompletedQuest)
	questBoardRoutes.get("/abandonQuest/:id", questBoardController.abandonQuest)
	questBoardRoutes.get("/cancelQuest/:id", questBoardController.cancelQuest)

	chatRoomRoutes.post("/messages",chatRoomController.postMessage)
	chatRoomRoutes.get("/messages/:id",chatRoomController.getMessage)
}
