import {Bearer} from 'permit';
import  jwtSimple from 'jwt-simple';
import { userService } from './main';
import { User } from './services/models';

import dotenv from "dotenv";
import {Request,Response,NextFunction} from "express";
import jwt from './jwt';

dotenv.config();

const permit = new Bearer({
    query:"access_token"
})

export async function isLoggedIn(req:Request,res:Response,next:NextFunction){
    try {
        const token = permit.check(req);
		// console.log(token)
        if (!token) {
            return res.status(401).json({msg: "Permission Denied"});
        }
        const payload = jwtSimple.decode(token, jwt.jwtSecret);
        const user:User = (await userService.getUserById(payload.id))[0];
		// console.log(payload)
        if (user) {
            req.user = user;
			// console.log(user)
            return next();
        } else {
            return res.status(401).json({msg: "Permission Denied"});
        }
    } catch (e) {
        return res.status(401).json({msg: "Permission Denied"});
    }
}


