## Redux

- useEffect

一開嗰個頁面就會行嘅function, 根據dependency array有轉變先行

```Typescript
useEffect(() => {
	...
},[dependency array])
```

###### dispatch係reload嘅時候會變返做null

useDispatch係react專用,`actions.ts`唔適合用const dispatch = useDispatch(), 而係用import {Dispatch} from "redux"

- useParams(用嚟攞冒號嘅地方)

可以攞到條route最後嘅params

eg

```Bash
http://localhost:3000/chatroom/4
```

呢個時候個params就係4, chatroom唔當係params嘅原因係睇返`App.tsx`

`App.tsx`

```Typescript
<Route exact path="/chatroom/:id"><PrivateChatRoom /></Route>
```

呢到個route寫咗條path係/chatroom/:id, 所以有冒號嘅地方就為params嘅地方, params係攞晒chatroom/打後嘅嘢

### Server

- [ ] config `routes.ts`

```Typescript
chatRoomRoutes.get("/messages/:id",chatRoomController.getMessage)
```

###### post,put,delete can use req.body, get cannot use

###### :id用嚟攞req.params.id

- [ ] config(修改) `controller`

```Typescript
getMessage = async(req:Request,res:Response) => {
	try{
		const receiverId = req.params.id;
		const message = await this.chatRoomService.getMessage(req.user?.id,parseInt(receiverId))
		if(message){
			res.status(200).json({success:true,message})
		}else{
			res.status(400).json({success:false,message:"User does not exist"})
		}
	}catch(e){
		logger.error(`path:${req.path} method:${req.method}`)
		logger.error(e)
		res.status(500).json({success:false,message:e.toString()})
	}
}
```

- [ ] config `service`

```Typescript
async getMessage(userId:number|undefined,receiverID:number){
	if(userId){
		const sentMessageByUser = await this.knex.select("content","created_at").from("chat").where({sent_user:userId,received_user:receiverID}).orderBy("created_at");
		const receivedMessageByUser = await this.knex.select("content","created_at").from("chat").where({sent_user:receiverID,received_user:userId}).orderBy("created_at");
		return {sent:sentMessageByUser,received:receivedMessageByUser}
	}
	return null
}
```

### Frontend

`export type IChatRoomActions = ReturnType<typeof sendMessageSuccess|typeof getMessageSuccess|typeof failed>` this line is important, remember to update the typeof when adding new function

#### Thunks

```Typescript
export function getMessage(id:number){
	return async(dispatch:Dispatch<IChatRoomActions>) => {
		const token = localStorage.getItem('token')
		const res = await fetch(`${process.env.REACT_APP_BACKEND_HOST}/chatroom/messages/${id}`,{
			headers:{
				Authorization:`Bearer ${token}`
			}
		})
		const result = await res.json()
		if(result.success){
			dispatch(getMessageSuccess(result.message))
		}else{
			dispatch(failed("Get_message_failed",result.message))
		}
		// console.log(result)
	}
}
```

- [ ] actions

`getMessageSuccess`

- [ ] reducer

for update store