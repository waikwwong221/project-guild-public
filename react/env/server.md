## server

- [ ] knex

- [ ] command (powershell/terminal [not vscode terminal])

```Bash
cd server
yarn add ts-node typescript @types/node ts-node-dev express @types/express cors @types/cors
```

- [ ] config `main.ts`

```Typescript
import express from "express";
import Knex from "knex";
import dotenv from "dotenv";
import cors from "cors";
const knexfile = require("./knexfile");
const knex = Knex(knexfile[process.env.NODE_ENV || "development"]);
dotenv.config();

const app = express();
app.use(cors());
app.use(express.urlencoded({extended:true}));
app.use(express.json());


const port = 9130;
app.listen(port,function(){
	console.log(`Listening at http://localhost:${port}`)
})
```

- config `routes.ts`

```Typescript
import express from "express";
import {userController} from "./main";

export const userRoutes = express.Router();

export function initRoutes(){
	userRoutes.post("/login",userController.login);
	userRoutes.get("/currentUsers",userController.currentUsers);
	userRoutes.post("/register",userController.register);
	userRoutes.post("/passwordByEmail",userController.resetPasswordByEmail);
	userRoutes.post("/googleLogin",userController.googleLogin);
}
```

- [ ] import `routes.ts` to `main.ts`

```Typescript
...
import {userRoutes,initRoutes} from "./routes";
...
initRoutes();
app.use("/",userRoutes);
...
```

- [ ] config `controller` and `services`

	- [ ] contoller

	```Typescript
	import {UserService} from "../services/UserService";
	import {Request,Response} from "express";
	import {checkPassword, hashPassword} from "../hash";
	import jwtSimple from "jwt-simple";
	import jwt from "../jwt";
	import {logger} from "../logger";
	import {User} from "../services/models";
	import nodemailer from "nodemailer";
	import fetch from "node-fetch";
	import crypto from "crypto";

	export class UserController{
		constructor(private userService:UserService){}

		login = async(req:Request,res:Response) => {
			try{
				const {username,password} = req.body;
				const user:User = (await this.userService.login(username))[0];

				if(!user){
					return res.status(400).json({message:"User does not exist"})
				}

				if(!(await checkPassword(password,user.password))){
					return res.status(400).json({message:"Password is incorrect"})
				}

				const token = jwtSimple.encode({
					id:user.id,
					username,
					password
				},jwt.JWT_SECRET)

				return res.status(200).json({success:true,data:token})
			}catch(e){
				logger.error(`path:${req.path} method:${req.method}`);
				logger.error(e);
				return res.status(500).json({success:false,message:e.toString()});
			}
		}

		currentUsers = async(req:Request,res:Response) => {
			try{
				const user:User[] = await this.userService.currentUsers();
				return res.status(200).json({success:true,data:user})
			}catch(e){
				logger.error(`path:${req.path} method:${req.method}`);
				logger.error(e);
				return res.status(500).json({success:false,message:e.toString()});
			}
		}

		register = async(req:Request,res:Response) => {
			try{
				const {username,password,name,email,phone,suggestedPhone} = req.body;
				let hashedPassword = await hashPassword(password);
				await this.userService.createUser(username,hashedPassword,name,email,phone,suggestedPhone);
				return res.status(200).json({success:true})
			}catch(e){
				logger.error(`path:${req.path} method:${req.method}`);
				logger.error(e);
				return res.status(500).json({success:false,message:e.toString()});
			}
		}

		resetPasswordByEmail = async(req:Request,res:Response) => {
			try{
				let user = (await this.userService.resetPasswordByEmail(req.body.email))[0];
				if(!user){
					return res.status(400).json({message:"This email is not registerd"})
				}
				const transporter = nodemailer.createTransport({
					host: "smtp.gmail.com",
					port: 587,
					secure: false,
					auth: {
						user: process.env.GMAIL_USER,
						pass: process.env.GMAIL_PASS,
					},
				});
				let info;
				info = await transporter.sendMail({
					from: "emojichattecky@gmail.com", // sender address
					to: user.email, // list of receivers
					subject: "重設密碼", // Subject line
					text: `Dear ${user.username},\nClick the following link to reset your password\n${process.env.FRONTEND_PORT}/resetPassword?uuid=${user.uuid}\n`,
				});
				// send mail with defined transport object
				logger.debug(info);
				return res.status(200).json({success:true})
			}catch(e){
				logger.error(`path:${req.path} method:${req.method}`);
				logger.error(e);
				return res.status(500).json({success:false,message:e.toString()});
			}
		}

		googleLogin = async(req:Request,res:Response) => {
			try{
				const fetchRes = await fetch("https://www.googleapis.com/oauth2/v2/userinfo", {
					method:"get",
					headers:{
						Authorization: `Bearer ${req.body.accessToken}`,
					},
				});
				const result = await fetchRes.json();

				if(!result){
					return res.status(400).json({message:"User does not exist"})
				}
				let token;
				let user = (await this.userService.login(result.name))[0];
				let loginByEmail = (await this.userService.loginByEmail(result.email))[0];
				logger.debug(user);
				if (!user && !loginByEmail) {
					user = {
						username: result.name,
						password: await hashPassword(crypto.randomBytes(30).toString()),
					};
					user.id = (await this.userService.createSocialUser(user.username,user.password,result.email))[0];
					req.user = user;
				}else if(user){
					req.user = user;
				}

				if(user){
					token = jwtSimple.encode({
						id:user.id,
						username:user.username,
						password:user.password
					},jwt.JWT_SECRET)
					return res.status(200).json({success:true,id:user.id,data:token});
				}else{
					token = jwtSimple.encode({
						id:loginByEmail.id,
						username:loginByEmail.username,
						password:loginByEmail.password
					},jwt.JWT_SECRET)
					return res.status(200).json({success:true,id:loginByEmail.id,data:token});
				}
			}catch(e){
				logger.error(`path:${req.path} method:${req.method}`);
				logger.error(e);
				return res.status(500).json({message:e.toString()});
			}
		}
	}
	```

	- [ ] services

	```Typescript
	import {Knex} from "knex";
	import {uuidGenerator} from "../main";

	export class UserService{
		constructor(private knex:Knex){}

		async login(username:string){
			return await this.knex.select("*").from("users").where("username",username);
		}

		async loginByEmail(email:string){
			return await this.knex.select("*").from("users").where("email",email);
		}

		async currentUsers(){
			return await this.knex.select("*").from("users");
		}

		async createUser(username:string,password:string,name:string,email:string,phone:number,suggestedPhone:number){
			const uuid = await uuidGenerator();
			await this.knex.insert({username,password,name,email,phone,suggestedPhone,uuid}).into("users");
		}

		async resetPasswordByEmail(email:string){
			return await this.knex.select("*").from("users").where("email",email);
		}

		async createSocialUser(username:string,password:string,email:string):Promise<number>{
			const uuid = await uuidGenerator();
			return await this.knex.insert({username,password,email,phone:12345678,uuid}).into("users").returning("id");
		}

		async getUserById(id:number){
			return await this.knex.select("*").from("users").where("id",id);
		}
	}
	```

	![folder](folder.png)