## Socket IO

- [ ] input useState

example

```Typescript
...
const [chatroomMessage,setChatroomMessage] = useState("");
...
<input type="text" placeholder="Send messages" value={chatroomMessage} onChange={event=>setChatroomMessage(event.currentTarget.value)}/>
<input type="submit"/>
```

- [ ] useDispatch to fetch message to server

```Typescript
...
const dispatch = useDispatch(); // 想喺actions到做嘢就要用
...
<input type="submit" onClick={()=>{
	dispatch(sendMessage(chatroomMessage)) // 呢個位用dispatch,寫埋你想用嘅function,而呢個function會寫喺actions到嘅
}}/>
...
```

- [ ] config `actions.ts` for fetch嘢去server

```Typescript
import {Dispatch} from "redux"; // 呢個位都要用dispatch

export function sendMessage(message:string){
	return async (dispatch:Dispatch<IChatRoomActions>) => {
		const token = localStorage.getItem('token')
		const res = await fetch(`${process.env.REACT_APP_BACKEND_HOST}/ chatroom/messages`,{ // 所以呢到要有variable `res`
			method:"post",
			headers:{
				Authorization:`Bearer ${token}`,
				"Content-type":"application/json"
			},
			body: JSON.stringify({message})
		})
		const result = await res.json(); // 攞返server send去frontend嘅嘢
	}
}
```

- [ ] command

```Bash
yarn add socket.io-client@4.0.0 @types/socket.io-client@1.4.36
```

- 到呢到就代表已經成功send嘢上server

### server

- [ ] config `main.ts`

socket.on代表收信息, io.emit代表send信息 (io代表班房,socket就代表同學)