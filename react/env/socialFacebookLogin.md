## Social Facebook Login

- [ ] img src need to import

- [ ] terminal

```Bash
import react-facebook-login @types/react-facebook-login
```

- [ ] example

```Typescript
<ReactFacebookLogin
	appId={process.env.REACT_APP_FACEBOOK_APP_ID || ""}
	autoLoad={false}
	fields="name,email,picture,birthday"
	callback={async(user:ReactFacebookLoginInfo) => {
		const {accessToken} = user
		console.log(user)
		const res = await fetch(`${process.env.REACT_APP_BACKEND_HOST}/facebookLogin`,{
			method:"post",
			headers:{
				"Content-Type":"application/json"
			},
			body:JSON.stringify({accessToken})
		})
		const result = await res.json();
		if(result.success){
			dispatch(loginSuccess(result.id,result.token))
		}
	}}
/>
```

- [ ] `https://developers.facebook.com/`

- [ ] create your app

- [ ] server

```Typescript
facebookLogin = async(req:Request,res:Response) => {
try{
	// console.log(req.body)
	const fetchRes = await fetch(`https://graph.facebook.com/me?access_token=${req.body.accessToken}`, {
		method:"get",
		headers:{
			Authorization: `Bearer ${req.body.accessToken}`,
		},
	});
	const result = await fetchRes.json();
	// console.log(result)
	if(!result){
		res.status(400).json({success:false,message:"User does not exist"})
	}
	let token;
	let user = (await this.userService.login(result.name))[0];
	if(result.email){
		let loginByEmail = (await this.userService.loginByEmail(result.email))[0];
		// logger.debug(user);
		if (!user && !loginByEmail) {
			user = {
				username: result.name,
				password: await hashPassword(crypto.randomBytes(30).toString()),
			}
			user.id = (await this.userService.createSocialUser(user.username,user.password,result.email))[0];
			req.user = user;
		}else if(user){
			req.user = user;
		}else if(loginByEmail){
			req.user = user;
		}

		if(user){
			token = jwtSimple.encode({
				id:user.id,
				username:user.username,
				password:user.password
			},jwt.jwtSecret)
			res.status(200).json({success:true,id:user.id,data:token});
		}else{
			token = jwtSimple.encode({
				id:loginByEmail.id,
				username:loginByEmail.username,
				password:loginByEmail.password
			},jwt.jwtSecret)
			res.status(200).json({success:true,id:loginByEmail.id,data:token});
		}
	}else{
		if (!user) {
			user = {
				username: result.name,
				password: await hashPassword(crypto.randomBytes(30).toString()),
			}
			user.id = (await this.userService.createSocialUserWithoutEmail(user.username,user.password))[0];
			req.user = user;
		}else if(user){
			req.user = user;
		}

		token = jwtSimple.encode({
			id:user.id,
			username:user.username,
			password:user.password
		},jwt.jwtSecret)
		res.status(200).json({success:true,id:user.id,data:token});
	}
}catch(e){
	logger.error(`path:${req.path} method:${req.method}`);
	logger.error(e);
	res.status(500).json({message:e.toString()});
}
```

- [ ] frontend can receive the jwt token

