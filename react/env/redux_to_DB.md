## Send thing to server

- [ ] write dispatch in tsx file

example (Register.tsx)

```Typescript
...
<TouchableOpacity onPress={()=>{
	checkDuplicate();
	// here
	dispatch(register(username,password,name,email,parseInt(phone),parseInt(suggestedPhone)))
}}>
...
```

- [ ] this dispatch action (`register`) can be found in `./redux/user/thunks.ts`

```Typescript
export function register(username:string,password:string,name:string,email:string,phone:number,suggestedPhone:number){
	return async(dispatch:Dispatch<IUserActions>) => {
		const res = await fetch(`${envfile.REACT_APP_BACKEND_PORT}/users/register`,{
			method:"post",
			headers:{
				"Content-Type":"application/json",
			},
			body:JSON.stringify({username,password,name,email,phone,suggestedPhone})
		})
		const result = await res.json();
		if(result.success){
			// this will go to `action.ts`
			dispatch(registerSuccess())
		}else{
			// this will go to `action.ts`
			dispatch(registerFailure(result.message));
		}
	}
}
```

- [ ] after fetch to server and do the function in `./redux/user/actions.ts`

```Typescript
export function registerSuccess(){
	return{
		type:"@@user/Register_success" as const,
	}
}

export function registerFailure(message:string){
	return{
		type:"@@user/Register_failure" as const,
		message
	}
}
...
export type IUserActions = ReturnType<typeof registerSuccess|typeof registerFailure|typeof registerNullSuccess|typeof resetPasswordSuccess|
typeof resetPasswordFailure|typeof resetNullSuccess>
```

###### remember add the new function type in ReturnType

exmaple (add a redeem gift success function)

```Typescript
export function redeemSuccess(){
	return{
		type:"@@gifts/Redeem_success" as const, // as const is a must to write
	}
}
...
export IGiftActions = ReturnType<typeof redeemSuccess>
```

- [ ] the action will be shown in `reducer.ts` to see what is required to do for this function

example (`src/user/reducer.ts`)

```Typescript
const userReducer = (state:IUserState = initialState,action:IUserActions):IUserState => {
	switch(action.type){
		case "@@user/Register_success":
			return{
				...state,
				isRegistered:true
			}
		case "@@user/Register_failure":
			return{
				...state,
				isRegistered:false,
				message:action.message
			}
		...
		// default is a must
		default:
			return state
	}
}

// remember to export the reducer
export default userReducer;
```