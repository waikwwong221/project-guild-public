## Knex migarations and seeds

### command line typed in Powershell (Windows) / terminal (Mac / Linux)

1. command line

```Bash
yarn add knex @types/knex pg @types/pg dotenv
```

2. command line

```Bash
yarn knex init -x ts
```

3. config `knexfile.ts`

```Typescript
// Update with your config settings.
import dotenv from "dotenv";
dotenv.config();

module.exports = {
    development: {
        client: "postgresql",
        connection: {
            database: process.env.POSTGRES_DB,
            user: process.env.DB_USER,
            password: process.env.POSTGRES_PASSWORD,
			host: process.env.DB_HOST,
			port: process.env.DB_PORT
        },
        pool: {
            min: 2,
            max: 10,
        },
        migrations: {
            tableName: "knex_migrations",
        },
    },

    staging: {
        client: "postgresql",
        connection: {
            database: "my_db",
            user: "username",
            password: "password",
        },
        pool: {
            min: 2,
            max: 10,
        },
        migrations: {
            tableName: "knex_migrations",
        },
    },

    production: {
        client: "postgresql",
        connection: {
            database: "my_db",
            user: "username",
            password: "password",
        },
        pool: {
            min: 2,
            max: 10,
        },
        migrations: {
            tableName: "knex_migrations",
        },
    },
}
```

4. config `.env`  ,open by yourself

```Typescript
DB_NAME=
DB_USERNAME=
DB_PASSWORD=
TEST_DB=
```

5. command line (migrations)

```Bash
yarn knex migrate:make create-{table.name}
```

6. config `migrations`

7. command line (seeds)

```Bash
yarn knex seed:make -x ts new-{entries.name}
```

8. config `seeds`

Sample

```Typescript
import { Knex } from "knex";
import {hashPassword} from "../hash";

export async function seed(knex: Knex): Promise<void> {
    // Deletes ALL existing entries
    await knex("users").del().where("username","ttiimmothhy");

    // Inserts seed entries
    await knex.insert([
		{
			username:"ttiimmothhy",
			password:await hashPassword("ttiimmothhy"),
			name:"Timothy Li",
			email:"timothy@timothy.io",
			phone:11111111,
			suggestedPhone:11111111
		}
	]).into("users");
}
```

9. command line (create database)

Option 1

```Bash
createdb {database.name} # same as the database name set in `.env`
```

Option 2

Step 1

```Bash
psql
```

Step 2

```Bash
create database {database.name}; # same as the database name set in `.env`
```

10. command line (create tables)

```Bash
yarn knex migrate:latest
```

11. command line (create initial data)

```Bash
yarn knex seed:run
```