## Check redux

```Typescript
import logger from "redux-logger";
// miss other imports, remember to write yourself

// for checking redux
declare global {
    /* tslint:disable:interface-name */
    interface Window {
        __REDUX_DEVTOOLS_EXTENSION_COMPOSE__: any
    }
}

// for checking redux
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
...
export const store = createStore(reducer,composeEnhancers(
	applyMiddleware(thunk),
	...
	applyMiddleware(logger)
))
```