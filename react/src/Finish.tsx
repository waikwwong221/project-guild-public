import React, { useEffect } from 'react'
import { NavLink, useParams, useRouteMatch } from 'react-router-dom'
import './Finish.css'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faArrowLeft } from '@fortawesome/free-solid-svg-icons'
import { confirmAlert } from 'react-confirm-alert'; // Import
import 'react-confirm-alert/src/react-confirm-alert.css'; // Import css
import { push } from 'connected-react-router'
import { useDispatch, useSelector } from 'react-redux'
import { fetchPublishedQuest, reportCompletedQuestBoard} from './redux/QuestBoard/action'
import { fetchUserData } from './redux/UserData/action'
import { fetchQuestBoardAcceptedQuest } from './redux/QuestBoardAcceptedQuest/action'
import { RootState } from './store'
// import completed from './photo/completed.png'
import Finished from './photo/Finished.png'
import Published from './photo/Published.png'

export function Finish() {
    const dispatch = useDispatch();
    useEffect(() => {
        dispatch(fetchPublishedQuest())
        dispatch(fetchUserData())
        dispatch(fetchQuestBoardAcceptedQuest())
    }, [dispatch])
    const QuestBoard = useSelector((state: RootState) => state.questBoard.questBoard)
    const router = useRouteMatch<{id?: string}>()
    const currentQuestBoard = QuestBoard.find(c => c.quest_id === parseInt(router.params.id ?? ''))
    const params = useParams<{ id: string }>();

    const userId = useSelector((state: RootState) => state.auth.userId)
    const questBoardAcceptedQuest = useSelector((state: RootState) => state.questBoardAcceptedState.questBoardAcceptedQuest);
    const LoginUserAcceptedQuest = questBoardAcceptedQuest.filter(c => c.took_by_user === userId)
    const acceptedQuestUser = LoginUserAcceptedQuest.filter(c => c.quest_id === parseInt(router.params.id ?? '')).length > 0
    const publishedQuestUser = currentQuestBoard?.user_id === userId
    const FinishedUser = QuestBoard.find(c => c.user_id === currentQuestBoard?.took_by_user)

    const confirmQuest = currentQuestBoard?.stage === "completed"

    if (confirmQuest) {
        dispatch(push("/completedQuest/" + currentQuestBoard?.quest_id))
    }

    for(let c of QuestBoard){
        if (c.difficulty === 1) {
            c.difficulty = "★☆☆☆☆"
        } else if (c.difficulty === 2) {
            c.difficulty = "★★☆☆☆"
        } else if (c.difficulty === 3) {
            c.difficulty = "★★★☆☆"
        } else if (c.difficulty === 4) {
            c.difficulty = "★★★★☆"
        } else if (c.difficulty === 5) {
            c.difficulty = "★★★★★"
        }
        if (c.bail === null) {
            c.bail = 0
        }
        if (c.users_photo === null) {
            c.users_photo = "nullUserIcon.png"
        }
        if (c.display_name === null) {
            c.display_name = `User ${c.id}`
        }
    }

    return (
         <div className="Finish-Display">
            <div className="UserPage-BackMain">
                <NavLink to="/main" style={{textDecoration: 'none'}}>
                    <div className="UserPage-Main-Button">
                        <div className="UserPage-Main-Button-fa"><FontAwesomeIcon icon={faArrowLeft} /></div>
                        <div className="UserPage-Main-Button-Title">Main Menu</div>
                    </div>
                </NavLink>
            </div>
            <div className="Finish-Title">Finish the Quest</div>
            {acceptedQuestUser &&
                <div className="Finish-Title-Content">Your have reported a finished quest. The quest publisher is confirming your work and you will get reward soon! </div>
            }
            {publishedQuestUser &&
                <div className="Finish-Title-Content">Your published quest have been finished! Confirm the result and leave a comment!</div>
            }
            <div className="Finish-Content-Display">
                <div className="Finish-Content-Title">About the Quest</div>
                <div className="Finish-Content-Quest-Publisher">Publisher: {currentQuestBoard?.display_name}<img className="QuestBoardId-img" src={`${process.env.REACT_APP_BACKEND_HOST}/${currentQuestBoard?.users_photo}`} alt=""></img></div>
                <div className="Finish-Content-Quest-Title">{currentQuestBoard?.quest_name}</div>
                <div className="Finish-Content-Quest-Prize">Prize: ${currentQuestBoard?.prize}</div>
                <div className="Finish-Content-Quest-Star">Difficulty <div className="QuestBoardId-Difficulty-star">{currentQuestBoard?.difficulty}</div></div>
            </div>
            <div className="Finished-User">Finished by user: {FinishedUser?.display_name}<img className="QuestBoardId-img" src={`${process.env.REACT_APP_BACKEND_HOST}/${FinishedUser?.users_photo}`} alt=""></img></div>
            {publishedQuestUser &&
                <div className="Finish-Confirm" onClick={() => {
                    confirmAlert({
                        title: 'Confirm to submit',
                        message: 'Are you sure Confirm This Quest?',
                        buttons: [
                        {
                            label: 'Yes',
                            onClick: () => {
                                dispatch(reportCompletedQuestBoard(parseInt(params.id)))
                                dispatch(push("/completedQuest/" + currentQuestBoard?.quest_id))
                            }
                        },
                        {
                            label: 'No',
                            onClick: () => {}
                        }
                        ]
                    });
                }}>Confirm</div>
            }
            <div className="Finished-Img">
                <img className="Img-Finished" src={Finished} alt=""></img>
                <img className="Img-Published" src={Published} alt=""></img>
            </div>
         </div>
     )
}
