import {AnyAction,applyMiddleware,combineReducers,compose,createStore} from 'redux'
import {RouterState,connectRouter,routerMiddleware,} from 'connected-react-router';
import {createBrowserHistory} from 'history';
import thunk,{ThunkDispatch as OldThunkDispatch} from 'redux-thunk';
import {authReducer,AuthState} from './redux/auth/reducer';
import {menuReducer,MenuState} from './redux/menu/reducer';
import {questBoardReducer,QuestBoardState} from './redux/QuestBoard/reducer';
import logger from "redux-logger";
import registerReducer,{RegisterState} from './redux/register/reducer';
import {userDataReducer,UserDataState} from './redux/UserData/reducer';
import {LoginMessageReducer,LoginMessageState} from './redux/LoginMessage/reducer';
import processReducer, { ProcessState } from './redux/Process/reducer';
import questBoardAcceptedQuestReducer, { QuestBoardAcceptedQuestState } from './redux/QuestBoardAcceptedQuest/reducer';
import chatRoomReducer,{IChatRoomState} from "./redux/chatRoom/reducer";

export const history = createBrowserHistory()

export type ThunkDispatch = OldThunkDispatch<RootState, null, AnyAction>;

export interface RootState {
    router: RouterState,
    auth: AuthState,
    menu: MenuState,
    questBoard: QuestBoardState
    register: RegisterState,
    userData: UserDataState,
    loginMessage: LoginMessageState,
    process:ProcessState,
    questBoardAcceptedState: QuestBoardAcceptedQuestState;
	chatRoom:IChatRoomState
}

const reducer = combineReducers<RootState>({
    router: connectRouter(history),
    auth: authReducer,
    menu: menuReducer,
    questBoard: questBoardReducer,
    register: registerReducer,
    userData: userDataReducer,
    process: processReducer,
    loginMessage: LoginMessageReducer,
    questBoardAcceptedState: questBoardAcceptedQuestReducer,
	chatRoom:chatRoomReducer
})

declare global {
    /* tslint:disable:interface-name */
    interface Window {
        __REDUX_DEVTOOLS_EXTENSION_COMPOSE__: any
    }
}

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export const store = createStore(reducer, composeEnhancers(
    applyMiddleware(thunk),
    applyMiddleware(routerMiddleware(history)),
    applyMiddleware(logger)
))