import { useDispatch, useSelector } from "react-redux"
import { useRouteMatch } from "react-router"
import { RootState } from "./store"
import './QuestBoardDetail.css'
import { useEffect, useState } from "react"
import { AbandonQuest, CancelQuest, fetchPublishedQuest, reportFinishedQuestBoard, updateQuestBoard } from "./redux/QuestBoard/action"
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faArrowLeft } from '@fortawesome/free-solid-svg-icons'
import { NavLink, useParams } from 'react-router-dom'
import { fetchQuestBoardAcceptedQuest } from "./redux/QuestBoardAcceptedQuest/action"
import { fetchUserData } from "./redux/UserData/action"
import { push } from "connected-react-router"
// import completed from './photo/completed.png'
// import Finished from './photo/Finished.png'
import Published from './photo/Published.png'
import approved from './photo/approved.png'
import { confirmAlert } from 'react-confirm-alert'; // Import
import 'react-confirm-alert/src/react-confirm-alert.css'; // Import css

export function QuestBoardDetail() {
    const dispatch = useDispatch();
    useEffect(() => {
        dispatch(fetchPublishedQuest())
        dispatch(fetchUserData())
        dispatch(fetchQuestBoardAcceptedQuest())
    }, [dispatch])
    const QuestBoard = useSelector((state: RootState) => state.questBoard.questBoard)
    const router = useRouteMatch<{id?: string}>()
    const currentQuestBoard = QuestBoard.find(c => c.quest_id === parseInt(router.params.id ?? ''))
    const params = useParams<{ id: string }>();

    const userId = useSelector((state: RootState) => state.auth.userId)
    const questBoardAcceptedQuest = useSelector((state: RootState) => state.questBoardAcceptedState.questBoardAcceptedQuest);
    const LoginUserAcceptedQuest = questBoardAcceptedQuest.filter(c => c.took_by_user === userId)
    const acceptedQuestUser = LoginUserAcceptedQuest.filter(c => c.quest_id === parseInt(router.params.id ?? '')).length > 0
    const publishedQuestUser = currentQuestBoard?.user_id === userId
    const acceptedUser = QuestBoard.find(c => c.user_id === currentQuestBoard?.took_by_user)

    const finishQuest = currentQuestBoard?.stage === "finished"
    const publishedCompleted = currentQuestBoard?.stage === "completed" && currentQuestBoard.user_id === userId
    const acceptedCompleted = currentQuestBoard?.stage === "completed"

    if (finishQuest) {
        dispatch(push("/finishedQuest/" + currentQuestBoard?.quest_id))
    } else if (publishedCompleted || acceptedCompleted) {
        dispatch(push("/completed/" + currentQuestBoard?.quest_id))
    }

    for(let c of QuestBoard){
        if (c.difficulty === 1) {
            c.difficulty = "★☆☆☆☆"
        } else if (c.difficulty === 2) {
            c.difficulty = "★★☆☆☆"
        } else if (c.difficulty === 3) {
            c.difficulty = "★★★☆☆"
        } else if (c.difficulty === 4) {
            c.difficulty = "★★★★☆"
        } else if (c.difficulty === 5) {
            c.difficulty = "★★★★★"
        }
        if (c.bail === null) {
            c.bail = 0
        }
        if (c.users_photo === null) {
            c.users_photo = "nullUserIcon.png"
        }
        if (c.display_name === null) {
            c.display_name = `User ${c.id}`
        }
    }

    return (
        <div className="QuestBoardId-Display">
            {
                currentQuestBoard &&
                <div>
                    <div className="UserPage-BackMain">
                        <NavLink to="/main" style={{textDecoration: 'none'}}>
                            <div className="UserPage-Main-Button">
                                <div className="UserPage-Main-Button-fa"><FontAwesomeIcon icon={faArrowLeft} /></div>
                                <div className="UserPage-Main-Button-Title">Main Menu</div>
                            </div>
                        </NavLink>
                    </div>
                    <div className="QuestBoardId-About">
                        {acceptedQuestUser &&
                            <div className="QuestBoardId-Send-Message">
                                <div>You have taken the quest! Finish the quest and take the prize! ^^</div>
                            </div>
                        }
                        {publishedQuestUser &&
                            <div className="QuestBoardId-Send-Message">
                                <div>Your Application has been published. You will receive notification if someone apply this quest.</div>
                            </div>
                        }
                        {!acceptedQuestUser && !publishedQuestUser &&
                            <div className="QuestBoardId-About-Text">About this Quest</div>
                        }
                    </div>
                    <div className="QuestBoardId-UserDate">
                        <div>
                            <div className="QuestBoardId-Difficulty">Difficulty <div className="QuestBoardId-Difficulty-star">{currentQuestBoard.difficulty}</div></div>
                            <div className="QuestBoardId-Publisher-User">Publisher: {currentQuestBoard.display_name}
                                <img className="QuestBoardId-img" src={`${process.env.REACT_APP_BACKEND_HOST}/${currentQuestBoard.users_photo}`} alt=""></img>
							</div>
                        </div>
                        <div>
                            <div className="QuestBoardId-Publish-Date">Publish Date: {currentQuestBoard.publish_date?.substr(0, 10)}</div>
                            <div className="QuestBoardId-Expiration-Date">Expiration Date: {currentQuestBoard.expiration_date.substr(0, 10)}</div>
                        </div>
                    </div>
                    <div className="QuestBoardId-Content-Display">
                        <div className="QuestBoardId-Title">{currentQuestBoard.quest_name}</div>
                        <div className="QuestBoardId-Content-Box">
                            <div className="QuestBoardId-Content">
                                <div>{currentQuestBoard.about_quest}</div>
                                {acceptedQuestUser &&
                                    <img className="QuestBoardId-Img-approved" src={approved} alt=""></img>
                                }
                                {publishedQuestUser &&
                                    <img className="QuestBoardId-Img-Published" src={Published} alt=""></img>
                                }
                            </div>
                        </div>
                        <div className="QuestBoardId-Money-Display">
                            <div className="QuestBoardId-Content-BailMoney">BAIL: ${currentQuestBoard.bail}</div>
                            <div className="QuestBoardId-Content-PrizeMoney">PRIZE: ${currentQuestBoard.prize}</div>
                        </div>
                    </div>
                    {acceptedUser !== undefined &&
                        <div className="Finished-User">Accepted by user: {acceptedUser?.display_name}
                        <img className="QuestBoardId-img" src={`${process.env.REACT_APP_BACKEND_HOST}/${acceptedUser?.users_photo}`} alt=""></img></div>
                    }
                    {acceptedUser === undefined &&
                        <div className="Finished-User">( No User Accepted )</div>
                    }
                    <div className="QuestBoardId-Button">
                        <div className="QuestBoardId-Ask">
                            {acceptedQuestUser &&
                                <div onClick={() => {
                                    dispatch(push("/chatroom/" + currentQuestBoard.user_id))
                                }}>Talk to Publisher</div>
                            }
                            {publishedQuestUser &&
                                <div onClick={() => {

                                }}>See Application</div>
                            }
                            {!acceptedQuestUser && !publishedQuestUser &&
                                <div onClick={() => {
                                    dispatch(push("/chatroom/" + currentQuestBoard.user_id))
                                }}>Ask Question</div>
                            }
                        </div>
                        {acceptedQuestUser &&
                            <div className="QuestBoardId-Take-Cancel">
                                <div className="QuestBoardId-Cancel" onClick={() => {
                                    confirmAlert({
                                        title: 'Confirm to Abandon',
                                        message: 'Are you sure Abandon This Quest?',
                                        buttons: [
                                          {
                                            label: 'Yes',
                                            onClick: () => {
                                                dispatch(AbandonQuest(router.params.id))
                                                dispatch(push('/main'))
                                            }
                                          },
                                          {
                                            label: 'No',
                                            onClick: () => ""
                                          }
                                        ]
                                      });
                                }}>Abandon</div>
                            </div>
                        }
                        {publishedQuestUser &&
                            <div className="QuestBoardId-Take-Cancel">
                                <div className="QuestBoardId-Cancel" onClick={() => {
                                    confirmAlert({
                                        title: 'Confirm to Cancel',
                                        message: 'Are you sure Cancel This Quest?',
                                        buttons: [
                                          {
                                            label: 'Yes',
                                            onClick: () => {
                                                dispatch(CancelQuest(router.params.id))
                                                dispatch(push('/main'))
                                            }
                                          },
                                          {
                                            label: 'No',
                                            onClick: () => ""
                                          }
                                        ]
                                      });
                                }}>Cancel Quest</div>
                            </div>
                        }
                        {!acceptedQuestUser && !publishedQuestUser &&
                            <div className="QuestBoardId-Take">
                                <div className="QuestBoardId-TakeQuest" onClick={() => {
                                    confirmAlert({ /*take Quest */
                                        title: 'Confirm to submit',
                                        message: 'Are you sure Take This Quest?',
                                        buttons: [
                                          {
                                            label: 'Yes',
                                            onClick: () => {/*here */
                                                dispatch(updateQuestBoard(router.params.id))
                                                setTimeout(() => {
                                                    dispatch(push('/main'))
                                                },3000)
                                            }
                                          },
                                          {
                                            label: 'No',
                                            onClick: () => ""
                                          }
                                        ]
                                      });
                                }}>Take This Quest</div>
                            </div>
                        }
                    </div>
                    {acceptedQuestUser &&
                        <div className="QuestBoardId-Finish" onClick={() => {
                            confirmAlert({ /*Finish */
                                title: 'Confirm to submit',
                                message: 'Are you sure Finish This Quest?',
                                buttons: [
                                  {
                                    label: 'Yes',
                                    onClick: () => {
                                        dispatch(reportFinishedQuestBoard(parseInt(params.id)))
                                        dispatch(push("/finishedQuest/" + currentQuestBoard?.quest_id))
                                    }
                                  },
                                  {
                                    label: 'No',
                                    onClick: () => ""
                                  }
                                ]
                              });
                        }}>Finish!</div>
                    }
                </div>
            }
        </div>
    )
}