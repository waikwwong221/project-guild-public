import React, { useMemo } from 'react';
import './Main.css'
import { useDispatch, useSelector } from 'react-redux'
import { RootState } from './store'
import { useEffect } from 'react'
import { fetchUserData } from './redux/UserData/action'
import { fetchQuestBoardAcceptedQuest } from './redux/QuestBoardAcceptedQuest/action'
import { fetchPublishedQuest } from './redux/QuestBoard/action'
import { push } from 'connected-react-router'
import Carousel from 'nuka-carousel';


export function Main() {
    const dispatch = useDispatch()
    useEffect(() => {
        dispatch(fetchPublishedQuest())
        dispatch(fetchUserData())
        dispatch(fetchQuestBoardAcceptedQuest())
    },[dispatch])
    const userData = useSelector((state: RootState) => state.userData.userData)
    const userId = useSelector((state: RootState) => state.auth.userId)
    
    const questBoard = useSelector((state: RootState) => state.questBoard.questBoard);
    const questBoardAcceptedQuest = useSelector((state: RootState) => state.questBoardAcceptedState.questBoardAcceptedQuest);
    // lower case for variable
    const LoginUserAcceptedQuest = questBoardAcceptedQuest.filter(c => c.took_by_user === userId && c.stage !== "completed")
    const LoginUserPublishedQuest = questBoard.filter(c => c.user_id === userId && c.stage !== "completed" && c.stage !== "cancelled")
    
    // console.log("LoginUserAcceptedQuest", LoginUserAcceptedQuest)
    // console.log("LoginUserPublishedQuest", LoginUserPublishedQuest)
    
    const userDataTransformed = useMemo(()=>{
        const newUserData = userData.filter(c => c.id === userId);
        for (let c of newUserData) {
            if (c.rank === 1) {
                c.rank = "★☆☆☆☆"
            } else if (c.rank === 2) {
                c.rank = "★★☆☆☆"
            } else if (c.rank === 3) {
                c.rank = "★★★☆☆"
            } else if (c.rank === 4) {
                c.rank = "★★★★☆"
            } else if (c.rank === 5) {
                c.rank = "★★★★★"
            } else if (c.rank === 0) {
                c.rank = "☆☆☆☆☆"
            }
            if (c.photo === null) {
                c.photo = "nullUserIcon.png"
            }
            if (c.self_intro === null) {
                c.self_intro = "( 未有新增個人信息 )"
            }
            if (c.display_name === null) {
                c.display_name = `User ${c.id}`
            }
        }
        return newUserData;
    },[userData,userId]);
    for(let c of questBoard){
		if(c.difficulty === 1){
			c.difficulty = "★☆☆☆☆"
			}else if(c.difficulty === 2){
			c.difficulty = "★★☆☆☆"
			}else if(c.difficulty === 3){
			c.difficulty = "★★★☆☆"
			}else if(c.difficulty === 4){
			c.difficulty = "★★★★☆"
			}else if(c.difficulty === 5){
			c.difficulty = "★★★★★"
            }else if(c.difficulty === 0){
                c.difficulty = "☆☆☆☆☆"
		}
	}
    for(let c of questBoardAcceptedQuest){
		if(c.difficulty === 1){
			c.difficulty = "★☆☆☆☆"
			}else if(c.difficulty === 2){
			c.difficulty = "★★☆☆☆"
			}else if(c.difficulty === 3){
			c.difficulty = "★★★☆☆"
			}else if(c.difficulty === 4){
			c.difficulty = "★★★★☆"
			}else if(c.difficulty === 5){
			c.difficulty = "★★★★★"
            }else if(c.difficulty === 0){
                c.difficulty = "☆☆☆☆☆"
        }
	}
    return (
        <div className="Main-Display">
            {userDataTransformed.map((c, index) => {
                return (
                <div key={index} className="Main-UserDate-All-Display">
                    <div className="Main-UserDate-Display">
                        <div className="Main-UserDate-Top-Display">
                            <div className="Main-UserDate-User-Photo">
                                <img src={`${process.env.REACT_APP_BACKEND_HOST}/${c.photo}`} alt=""></img>
                            </div>
                            <div className="Main-UserDate">
                                <div className="Main-UserDate-UserName">{c.display_name}</div>
                                <div className="Main-UserDate-Content-Display">
                                    <div className="Main-UserDate-Content">{c.self_intro}</div>
                                </div>
                            </div>
                        </div>
                        <div className="Main-UserDate-Bottom-Display">
                            <div className="Main-UserDate-Rank-Display">{"🌟🌟🌟"}</div>
                            <div className="Main-UserDate-star-Display">
                                <div className="Main-UserDate-star">{Array(c.rank).fill('★').join("").padEnd(5,'☆')}</div>
                            </div>
                        </div>
                    </div>
                    <div className="Main-UserDate-Exp-Money-Display">
                        <div className="Main-UserDate-Balance">BALANCE: ${c.currency}</div>
                        <div className="Main-UserDate-Exp-level-Display">
                            <div>Event</div>
                            <div className="Main-UserDate-Exp">Exp 50% up !</div>
                            <div>04 June - 05 June</div>
                        </div>
                    </div>
                </div>
            )})}
            <div className="wardrobe_2">
                {LoginUserAcceptedQuest.length > 0 &&
                    <Carousel>
                    {LoginUserAcceptedQuest.map((c, index) => {
                        return(
                        <div key={index} className="Main-Accepted-Quests-Display">
                            <div className="Main-Accepted-Quests-Title">Accepted Quests</div>
                            <div className="Main-Accepted-Quests-Quest-Display">
                                <div className="Main-Accepted-Quests-Quest-Display-Box">
                                    <div>
                                        <div className="Main-Accepted-Quests-Star-Display">
                                            <div className="Main-Accepted-Quests-Quest-Title">{c.quest_name.substr(0, 10) + "..."}</div>
                                            <div className="Main-Accepted-Quests-Quest-Star">{c.difficulty}</div>
                                        </div>
                                        <div className="Main-Accepted-Quests-Quest-Content">
                                            <div>{c.about_quest.substr(0, 20) + "..."}</div>
                                        </div>
                                    </div>
                                    <div className="Main-Accepted-Quests-Quest-Details-Display">
                                        <div className="Main-Accepted-Quests-Quest-Prize">PRIZE: ${c.prize}</div>
                                        <div className="Main-Accepted-Quests-Quest-Details" onClick={() => {
                                            dispatch(push("/questBoard/" + c.quest_id))
                                        }}>Details</div>
                                    </div>
                                </div>
                                <div className="Main-Accepted-Quests-Quest-Photo">
                                <img className="Main-Accepted-Quests-Quest-Photo-Img" src={`${process.env.REACT_APP_BACKEND_HOST}/${c.photo}`} alt=""></img>
                                </div>
                            </div>
                        </div>
                    )})}
                    </Carousel>
                }
                {LoginUserPublishedQuest.length > 0 &&
                    <Carousel>
                    {LoginUserPublishedQuest.map((c, index) => {
                        return(
                        <div key={c.id} className="Main-Accepted-Quests-Display">
                            <div className="Main-Accepted-Quests-Title">Published Quests</div>
                            <div className="Main-Accepted-Quests-Quest-Display">
                                <div className="Main-Accepted-Quests-Quest-Display-Box">
                                    <div>
                                        <div className="Main-Accepted-Quests-Star-Display">
                                            <div className="Main-Accepted-Quests-Quest-Title">{c.quest_name.substr(0, 10) + "..."}</div>
                                            <div className="Main-Accepted-Quests-Quest-Star">{c.difficulty}</div>
                                        </div>
                                        <div className="Main-Accepted-Quests-Quest-Content">
                                            <div>{c.about_quest.substr(0, 20) + "..."}</div>
                                        </div>
                                    </div>
                                    <div className="Main-Accepted-Quests-Quest-Details-Display">
                                        <div className="Main-Accepted-Quests-Quest-Prize">PRIZE: ${c.prize}</div>
                                        {/* <Link to={"/questBoard/" + c.quest_id}> */}
                                            <div className="Main-Accepted-Quests-Quest-Details" onClick={() => {
                                                dispatch(push("/questBoard/" + c.quest_id))
                                            }}>Details</div>
                                        {/* </Link> */}
                                    </div>
                                </div>
                                <div className="Main-Accepted-Quests-Quest-Photo">
                                    <img className="Main-Accepted-Quests-Quest-Photo-Img" src={`${process.env.REACT_APP_BACKEND_HOST}/${c.quest_photo}`} alt=""></img>
                                </div>
                            </div>
                        </div>
                    )})}
                    </Carousel>
                }
            </div>
        </div>
    )
}
