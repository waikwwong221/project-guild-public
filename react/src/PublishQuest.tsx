import './PublishQuest.css';
import {useDispatch,useSelector} from 'react-redux';
import {publishQuest} from './redux/QuestBoard/action';
import {RootState} from './store';
import {useEffect, useState} from "react";
import {NavLink} from 'react-router-dom';
import {push} from 'connected-react-router'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faArrowLeft, faPlusCircle } from '@fortawesome/free-solid-svg-icons'
import { fetchUserData } from './redux/UserData/action';
import { useCurrentUserData } from './redux/UserData/hooks';

export interface PublishQuestDetails{
	quest_name:string;
	duration:string;
	expiration_date:string;
	userId:number;
	prize:string;
	bail:string;
	difficulty:string;
	photo:File|null|undefined;
	about_quest:string;
}

export function PublishQuest(){
	const dispatch = useDispatch();
	// Can use reactHookForm
	const [quest_name,setQuestName] = useState("");
	const [duration,setDuration] = useState("");
	const [expiration_date,setExpirationDate] = useState("");
	const [prize,setPrize] = useState("");
	const [bail,setBail] = useState("");
	const [difficulty,setDifficulty] = useState("");
	const [photo,setPhoto] = useState<File|null>();
	const [about_quest,setAboutQuest] = useState("");
	const [contentLength, setContentLength] = useState(0)

	// React custom hooks
	const [newUserData,userId] = useCurrentUserData();
	const userCurrency = (newUserData.find(c => c.currency)?.currency || "")

	return(
		<div className="PublishQuest-Display">
			<div className="UserPage-BackMain">
                <NavLink to="/main" style={{textDecoration: 'none'}}>
                    <div className="UserPage-Main-Button">
                        <div className="UserPage-Main-Button-fa"><FontAwesomeIcon icon={faArrowLeft} /></div>
                        <div className="UserPage-Main-Button-Title">Main Menu</div>
                    </div>
                </NavLink>
            </div>
			<div className="PublishQuest-Name">Publish Quest</div>
			<form onSubmit={(event) => {
				event.preventDefault();
				// Can be at different function
				if (userCurrency < parseInt(prize)){
					// console.log(userCurrency < parseInt(prize))
					alert("You can not published a quest that the prize over your curreny balance")
					return
				}
				dispatch(publishQuest({quest_name,duration,expiration_date,userId,prize,bail,difficulty,photo,about_quest}));
				alert("已成功發佈任務！")
				dispatch(push("/main"))
			}}>
				<div className="PublishQuest-Content-Display">
					<div className="PublishQuest-Title">
						<div>Quest Title</div>
						{/* <input {...register("quest_name")}/> */}
						<input value={quest_name} required onChange={(event) => {
							setQuestName(event.currentTarget.value)
						}}/>
					</div>
					<div className="PublishQuest-Date">
						<div>Duration</div>
						{/* <input {...register("duration")}/> */}
						<input value={duration} required onChange={(event) => {
							setDuration(event.currentTarget.value)
						}}/>
					</div>
					<div className="PublishQuest-LastDate">
						<div>Expiration Date</div>
						{/* <input type="date" {...register("expiretion_date")}/> */}
						<input type="date" value={expiration_date} required onChange={(event) => {
							setExpirationDate(event.currentTarget.value)
						}}/>
					</div>
					<div className="PublishQuest-Prize">
						<div className="PublishQuest-Prize-Prize">
							<div>Prize</div>
							{/* <input type="number" {...register("prize")}/> */}
							<input type="number" min="0" value={prize} required onChange={(event) => {
								setPrize(event.currentTarget.value)
							}}/>
						</div>
						<div className="PublishQuest-Prize-Bail">
							<div>Bail</div>
							{/* <input type="number" {...register("bail")}/> */}
							<input type="number" max="prize" value={bail} required onChange={(event) => {
								setBail(event.currentTarget.value);
							}}/>
						</div>
					</div>
					<div className="PublishQuest-Difficulty">
						<div>Difficulty</div>
						{/* <input type="number" min="0" max="5"{...register("difficulty")}/> */}
						<input type="number" value={difficulty} required min="0" max="5" onChange={(event) => {
							setDifficulty(event.currentTarget.value);
						}}/>
						<div className="PublishQuest-Star">☆☆☆☆☆</div>
					</div>
					{/* <div className="PublishQuest-People-Number">
                    <div>Num of Required People</div>
                    <input type="text"></input>
                	</div> */}
					<div className="PublishQuest-Content">
						<label>
							<div className="PublishQuest-UserIcon-Button-Display">
								<div className="PublishQuest-UserIcon-faPlusCircle"><FontAwesomeIcon icon={faPlusCircle} /></div>
								<div>Add Photo</div>
							</div>
							<input className="UserPage-UserIcon-file" type="file" accept="image/jpeg, image/png," onChange={(event) => {
								const files = event.currentTarget.files;
								if(files){
									setPhoto(files[0])
								}
							}}/>
						</label>
						<textarea rows={8} value={about_quest} required placeholder="Write down the Quest Details, Localtion, Requirement...etc"
						onChange={(event) => {
							setAboutQuest(event.currentTarget.value)
							setContentLength(event.currentTarget.value.length)
							if (event.currentTarget.value.length > 255) {

							}
						}}></textarea>
						<div className="PublishQuest-Content-Number">{contentLength}/255</div>
					</div>
				</div>
				<div className="PublishQuest-Button">
					<input className="PublishQuest-Button-Publish" type="submit" value="publish" />
					<NavLink to="/" activeClassName="PublishQuest-BackMain">
						<div className="PublishQuest-Button-Cancel">Cancel</div>
					</NavLink>
				</div>
			</form>
			<div>
				{/* for(let quest of QuestBorld){
					return(
						<div>
								{quest}
						</div>
					)
				} */}
				{/* {questBoard.map((quest,index) =>
					<div key={index}>{quest.about_quest}</div>
				)} */}
			</div>
		</div>
	)
}