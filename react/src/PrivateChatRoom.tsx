import { NavLink, useParams, useRouteMatch } from 'react-router-dom'
import './PrivateChatRoom.css'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faArrowLeft } from '@fortawesome/free-solid-svg-icons'
import { faImages } from '@fortawesome/free-solid-svg-icons'
import React,{ useEffect, useRef, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getMessage, sendMessage, updateSend } from "./redux/chatRoom/actions";
import io from 'socket.io-client';
import { RootState } from './store';
// import console from 'console';

export function PrivateChatRoom() {
    const userMessage = useSelector((state: RootState) => state.chatRoom.message)
    const [chatroomMessage, setChatroomMessage] = useState("");
    // const [chatHistory, setChatHistory] = useState<{ message: string, time: string }[]>([])
    const dispatch = useDispatch();
    const params = useParams<{ id: string }>();
    let [socket, setSocket] = useState<SocketIOClient.Socket | null>(null)
    const User = useSelector((state: RootState) => state.userData.userData)
	const userId = useSelector((state:RootState)=>state.auth.userId)
	// console.log(userId)
    const router = useRouteMatch<{ id?: string }>()
    const MatchUser = User.find(c => c.id === parseInt(router.params.id ?? ''))
	// console.log(userMessage)

    for (let c of User) {
        if (c.rank === 1) {
            c.rank = "★☆☆☆☆"
        } else if (c.rank === 2) {
            c.rank = "★★☆☆☆"
        } else if (c.rank === 3) {
            c.rank = "★★★☆☆"
        } else if (c.rank === 4) {
            c.rank = "★★★★☆"
        } else if (c.rank === 5) {
            c.rank = "★★★★★"
        }
        if (c.rank === 0) {
            c.rank = "☆☆☆☆☆"
        }
    }
	// console.log(send)

    useEffect(() => {
        const socket = io(`${process.env.REACT_APP_BACKEND_HOST}`);
        socket.on(`new-message-${userId}-${params.id}`, (message:{message:string,send:boolean}) => {
            let newChatHistory = userMessage.slice()
			if(message.send){
				newChatHistory.push({ content:message.message, created_at: (new Date()).toISOString(),sent_user:userId })
			}else if(params && params.id){
				newChatHistory.push({ content:message.message, created_at: (new Date()).toISOString(),sent_user:parseInt(params.id )})
			}

			// console.log(newChatHistory)
            dispatch(updateSend(newChatHistory))
        })
        setSocket(socket)
        // 當離開呢個page就會行呢步
        return () => {
            socket.disconnect();
        }
    }, [userMessage,params,dispatch,userId])

	useEffect(() => {
		dispatch(getMessage(parseInt(params.id)))
		// console.log("Chiu")
	},[dispatch,params.id])


    const chatRoomEl = useRef<HTMLDivElement|null>(null);
    useEffect(()=>{
        // Reactive programming
        if(chatRoomEl.current){
            const element = chatRoomEl.current;
            element.scrollTop = element.scrollHeight
        }
    },[userMessage,chatRoomEl])


    return (
        <div className="PrivateChatRoom-Display">
            {MatchUser &&
			<div>
                <div className="UserPage-BackMain">
                    <NavLink to="/chatroom" style={{ textDecoration: 'none' }}>
                        <div className="UserPage-Main-Button">
                            <div className="UserPage-Main-Button-fa"><FontAwesomeIcon icon={faArrowLeft} /></div>
                            <div className="UserPage-Main-Button-Title">Back to Contact</div>
                        </div>
                    </NavLink>
                </div>
                <div className="PrivateChatRoom-Content-Display">
                    <div className="PrivateChatRoom-Content-UserDate-Display">
                        <div className="PrivateChatRoom-Content-UserDate-Icon">
                            <img src={`${process.env.REACT_APP_BACKEND_HOST}/${MatchUser.photo}`} alt=""></img>
                        </div>
                        <div className="PrivateChatRoom-Content-UserDate-Username-Rank-Display">
                            <div className="PrivateChatRoom-Content-UserDate-Username">{MatchUser?.display_name} </div>
                            <div className="PrivateChatRoom-Content-UserDate-Rank">{MatchUser.rank}</div>
                        </div>
                    </div>
                    <div className="PrivateChatRoom-Content-Chatroom-Date-Display">
                        <div className="PrivateChatRoom-Content-Chatroom-Date" ref={chatRoomEl}>
                            {
								userMessage && userMessage.length > 0 && userMessage.map((history, index) => (
								<div key={index}>
									<div>
										{
											history.sent_user === userId && <div className="PrivateChatRoom-Content-Chatroom-Other-Message-Display">
												<div className="sent PrivateChatRoom-Content-Chatroom-My-Message">
													<div className="PrivateChatRoom-Content-Chatroom-Message">{history.content.slice(0, 32)}
														{history.content.slice(32)}
													</div>
													<div className="PrivateChatRoom-Content-Chatroom-Time">
														{(parseInt(history.created_at.slice(11,14)) + 8)}:{history.created_at.slice(14,16)}
													</div>
												</div>
											</div>
										}
										{
											params.id && history.sent_user === parseInt(params.id) &&
											<div className="PrivateChatRoom-Content-Chatroom-Other-Message">
												<div className="PrivateChatRoom-Content-Chatroom-Message">{history.content.slice(0, 32)} {history.content.slice(32)}</div>
												<div className="PrivateChatRoom-Content-Chatroom-Time">{(new Date(history.created_at).getHours()+"").padStart(2,"0")}:{history.created_at.slice(14,16)}</div>
											</div>
										}
									</div>
								</div>
								))
                            }
                        </div>
                        <form onSubmit={(event) => {
                            event.preventDefault();
                            if (chatroomMessage.trim().length) {
                                socket?.emit("message", {message:chatroomMessage,receiver:parseInt(params.id),sender:userId})
                                dispatch(sendMessage(parseInt(params.id), chatroomMessage))
                                setChatroomMessage(chatroomMessage => "")
                                
                            } 
                        }}>
                            <div className="PrivateChatRoom-Content-Chatroom-SendMessage">
                                <input className="PrivateChatRoom-Content-Chatroom-SendMessage-Text" type="text" placeholder="Send messages" value={chatroomMessage} onChange={(event) => {
                                    setChatroomMessage(event.currentTarget.value)
                                }}/>
                                <label className="PrivateChatRoom-Content-faImages">
                                    <FontAwesomeIcon icon={faImages} />
                                    <input className="PrivateChatRoom-Content-file" type="file"></input>
                                </label>
                                <input className="PrivateChatRoom-Content-Chatroom-SendMessage-Submit" type="submit" value="send"/>
                            </div>
                        </form>
                	</div>
            	</div>
				</div>
            }
        </div>
    )
}


// {params.id && history.sent_user === parseInt(params.id) &&
// 	<div>
// 		<div className="PrivateChatRoom-Content-Chatroom-Message">{history.content.slice(0, 32)} {history.content.slice(32)}</div>
// 		<div className="PrivateChatRoom-Content-Chatroom-Time">{history.created_at}</div>
// 	</div>}