import { push } from 'connected-react-router'
import { useDispatch, useSelector } from 'react-redux'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faArrowLeft } from '@fortawesome/free-solid-svg-icons'
import './ChatRoom.css'
import { useEffect } from 'react'
import { NavLink } from 'react-router-dom'
import { RootState } from './store'

export function ChatRoom() {
    const dispatch = useDispatch()
    const userId = useSelector((state:RootState)=>state.auth.userId)
    const User = useSelector((state: RootState) => state.userData.userData)

    return (
        <div className="ChatRoom-Display">
            <div className="UserPage-BackMain">
                <NavLink to="/main" style={{ textDecoration: 'none' }}>
                    <div className="UserPage-Main-Button">
                        <div className="UserPage-Main-Button-fa"><FontAwesomeIcon icon={faArrowLeft} className="Arrow" /></div>
                        <div className="UserPage-Main-Button-Title">Main Menu</div>
                    </div>
                </NavLink>
            </div>
            <div className="ChatRoom-Chat-Display">
                <div className="ChatRoom-Chat-Title-Search-Display">
                    <div className="ChatRoom-Chat-Title-Name">Other Users</div>
                    <div className="ChatRoom-Chat-Title-Search">
                        <input type="text" placeholder="🔍search"></input>
                    </div>
                </div>
                {User.filter(q=>q.id !== userId).map((q, index) => {
                    return (
                        <div key={index} className="ChatRoom-Chat" onClick={() => {
                            dispatch(push('/chatroom/' + q.id))
                        }}>
                            <div className="ChatRoom-Chat-Icon-RoomName-Display">
                                <div className="ChatRoom-Chat-Icon">
                                <img src={`${process.env.REACT_APP_BACKEND_HOST}/${q.photo}`} alt=""></img>
                                </div>
                                <div className="ChatRoom-Chat-RoomName-Message">
                                    <div className="ChatRoom-Chat-RoomName">{q.display_name}</div>
                                    <div className="ChatRoom-Chat-Message"></div>
                                </div>
                            </div>
                            <div className="ChatRoom-Chat-LastTame">19小時前</div>
                        </div>
                    )
                }
                )}
                
               
            </div>
        </div>
    )
}