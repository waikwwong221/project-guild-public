export function loginMessage(message: string) {
    return {
        type: '@@loginMessage/LOGIN_ERROR_MESSAGE' as const,
        errorMessage: message,
    }
}

export type LoginMessageActions = ReturnType<typeof loginMessage>