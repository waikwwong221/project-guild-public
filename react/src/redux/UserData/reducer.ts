import { UserDataActions } from "./action"
export interface UserData{
    id:number;
    username:string;
    password:string;
    email:string;
    currency:number;
    rank:number|string;
    taking_quest:number;
    created_at:string;
    photo:string;
    achievement:string;
    display_name:string;
    self_intro:string;
}
export interface UserDataState{
    userData:UserData[]
}
const initialState:UserDataState = {
    userData:[]
}
export const userDataReducer = (state: UserDataState = initialState, action: UserDataActions): UserDataState => {
    switch(action.type) {
        case "@@complaints/LOAD_USERDATA":
            return{
                ...state,
                userData:action.userData
            }
        default:
            return state
    }
}