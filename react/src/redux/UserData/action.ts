import { Dispatch } from "redux";
import { UserPage } from "../../UserPage";
// import { UserData } from "./reducer";

export function loadUserData(userData: any) {
    return {
        type: '@@complaints/LOAD_USERDATA' as const,
        userData
    }
}

export function fetchUserData() {
    return async (dispatch: Dispatch) => {
        // dispatch(loading())

        try {
            const res = await fetch(`${process.env.REACT_APP_BACKEND_HOST}/users/loadUserData`)
            const json = await res.json();

            dispatch(loadUserData(json))
        } catch (e) {
            // gracefully fail
            // dispatch(error())
        }
    }
}

export function userDataUpdate(display_name:string,self_intro:string,photo:File|null|undefined, userId:number) {
    return async (dispatch: Dispatch) => {
        try {
            const token = localStorage.getItem('token')
			let userDataUpdate = new FormData();
            if(photo){
				userDataUpdate.append("photo",photo);
			}
            userDataUpdate.append("display_name",display_name)
            userDataUpdate.append("self_intro",self_intro)

            const res = await fetch(`${process.env.REACT_APP_BACKEND_HOST}/users/userDataUpdate`,{
                method: "post",
                body: userDataUpdate,
                headers: {
                    Authorization:`Bearer ${token}`
                }
			})

            await res.json();
			// console.log(json)
            dispatch(UserPage())
        } catch (e) {
            // Didn't handle error
        }
    }
}

export type UserDataActions = ReturnType<typeof loadUserData>