import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../../store";
import { fetchUserData } from "./action";
import { UserData } from "./reducer";




export function useCurrentUserData():[UserData[],number]{
 
    const dispatch = useDispatch();
	useEffect(() => {
        dispatch(fetchUserData())
    }, [dispatch])

    const userData = useSelector((state: RootState) => state.userData.userData)
	const userId = useSelector((state: RootState) => state.auth.userId);
    const newUserData = userData.filter(c => c.id === userId)


    return [newUserData,userId];
}