import {ProcessActions} from "./action"

export interface Process{
    id?:number;
    stage?:string;
    quest_id:number;
    user_id?:number;
    created_at:string;
    updated_at:string;
    
}

export interface ProcessState{
    process:Process[]
}

const initialState:ProcessState = {
    process:[]
}

export const processReducer = (state: ProcessState = initialState, action: ProcessActions): ProcessState => {
    switch(action.type){
        case "@@complaints/LOAD_PROCESS":
            return{
                ...state,
                process:action.process
            }
        default:
            return state
    }
}
   

export default processReducer;