import { Dispatch } from "redux";

// action creator
export function loadProcess(process: any) {
    return {
        type: '@@complaints/LOAD_PROCESS' as const,
        process
    }
}

// thunk action creator
export function fetchProcess() {
    return async (dispatch: Dispatch) => {
        // dispatch(loading())

        try {
            const token = localStorage.getItem('token')
            const res = await fetch(`${process.env.REACT_APP_BACKEND_HOST}/acceptedQuest`,{
                headers:{
                    Authorization:`Bearer ${token}`
                }
            })
            const json = await res.json();
            console.log(json)

            dispatch(loadProcess(json))
        } catch (e) {
        }
    }
}


export type ProcessActions = ReturnType<typeof loadProcess>