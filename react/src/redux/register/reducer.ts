import { RegisterActions } from "./action";


// Not necessary as global state
// Only use global state if you need to share
export interface Register{
    username:string;
    password:string;
    email:string;
}

export interface RegisterState{
    register:Register[]
}

const initialState:RegisterState = {
    register:[]
}

export const registerReducer = (state: RegisterState = initialState, action: RegisterActions): RegisterState => {
    switch(action.type){
        case "@@quest/POST_REGISTER":
            let newRegister = state.register.slice();
            newRegister.push({
				username: action.registerDate.username,
				password: action.registerDate.password,
				email: action.registerDate.email,
			})
            return{
                ...state,
                register: newRegister
            }
        default:
            return state
    }
}

export default registerReducer;