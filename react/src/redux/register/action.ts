import { Dispatch } from "redux";
import { Register } from "./reducer";

export function registerSuccess(registerDate:Register){
    return{
        type:"@@quest/POST_REGISTER" as const,
        registerDate
    }
}
// naming: register, registerThunk
export function fetchRegister(registerDate:Register) {
    return async (dispatch: Dispatch) => {
        try {
            let register = new FormData();
            register.append("username",registerDate.username)
            register.append("password",registerDate.password)
            register.append("email",registerDate.email)

            const res = await fetch(`${process.env.REACT_APP_BACKEND_HOST}/users/register`,{
                method:"post",
                body:register,
            })
            const json = await res.json();
            if(json.success){
				dispatch(registerSuccess(registerDate))
			}
        } catch (e) {
            // gracefully fail
            // dispatch(error())
        }
    }
}

export type RegisterActions = ReturnType<typeof registerSuccess>