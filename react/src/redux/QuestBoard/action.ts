// import { push } from "connected-react-router";
import { Dispatch } from "redux";
import { PublishQuestDetails } from "../../PublishQuest";

// action creator
export function loadQuestBoard(questBoard: any) {
    return {
        type: '@@complaints/Load_quest_board' as const,
        questBoard
    }
}

export function publishQuestSuccess(questDetails:PublishQuestDetails){
    return{
        type:"@@quest/PUBLISH_QUEST" as const,
        questDetails
    }
}
// thunk action creator
export function fetchQuestBoard() {
    return async (dispatch: Dispatch) => {
        try {
            const token = localStorage.getItem('token')
			// console.log(token)
            const res = await fetch(`${process.env.REACT_APP_BACKEND_HOST}/questBoard`,{
                headers:{
                    Authorization:`Bearer ${token}`
                }
            })
            const json = await res.json();
            // console.log(json)
            dispatch(loadQuestBoard(json))
        } catch (e) {
            // gracefully fail
            // dispatch(error())
        }
    }
}

// thunk action creator
export function publishQuest(questDetails:PublishQuestDetails) {
    return async (dispatch: Dispatch) => {
        try{
            const token = localStorage.getItem('token')
			let publishQuestDetail = new FormData();
			publishQuestDetail.append("quest_name",questDetails.quest_name);
			publishQuestDetail.append("duration",questDetails.duration);
			publishQuestDetail.append("expiration_date",questDetails.expiration_date);
            publishQuestDetail.append("userId",questDetails.userId + "");
			publishQuestDetail.append("prize",questDetails.prize);
			publishQuestDetail.append("bail",questDetails.bail);
			publishQuestDetail.append("difficulty",questDetails.difficulty);
			publishQuestDetail.append("about_quest",questDetails.about_quest);
			if(questDetails.photo){
				publishQuestDetail.append("photo",questDetails.photo);
			}
            const res = await fetch(`${process.env.REACT_APP_BACKEND_HOST}/questBoard`,{
                method:"post",
                body:publishQuestDetail,
                headers:{
                    Authorization:`Bearer ${token}`
                }
			})
            const json = await res.json();
            // console.log(json)
			if(json.success){
				dispatch(publishQuestSuccess(questDetails))
			}
        } catch (e) {
            // no error handling
            // gracefully fail
            // dispatch(error())
        }
    }
}

export function fetchPublishedQuest() {
    return async (dispatch: Dispatch) => {
        try {
            const token = localStorage.getItem('token')
			// console.log(token)
            const res = await fetch(`${process.env.REACT_APP_BACKEND_HOST}/publishedQuest`,{
                headers:{
                    Authorization:`Bearer ${token}`
                }
            })
            const json = await res.json();
            // console.log(json)
            dispatch(loadQuestBoard(json))
        } catch (e) {
            // gracefully fail
            // dispatch(error())
        }
    }
}

export function updateQuestBoard(id:string|undefined){
    return async (dispatch: Dispatch) => {
        try{
            const token = localStorage.getItem('token')
            // console.log(token)
            if(id){
                await fetch(`${process.env.REACT_APP_BACKEND_HOST}/questBoardApplication/${id}`,{
                    headers:{
                        Authorization:`Bearer ${token}`
                    }
                })

                // const json = await res.json();
                // console.log(json)
            }
        } catch (e) {
            // gracefully fail
            // dispatch(error())
        }
    }
}

export function reportFinishedQuestBoard(id:number){
    return async (dispatch: Dispatch) => {
        try{
            const token = localStorage.getItem('token')
            // console.log(token)
            if(id){
                await fetch(`${process.env.REACT_APP_BACKEND_HOST}/reportFinishedQuest/${id}`,{
                    headers:{
                        Authorization:`Bearer ${token}`
                    }
                })

                // const json = await res.json();
                // console.log(json)
            }
        } catch (e) {
            // gracefully fail
            // dispatch(error())
        }
    }
}
export function reportCompletedQuestBoard(id:number){
    return async (dispatch: Dispatch) => {
        try{
            const token = localStorage.getItem('token')
            // console.log(token)
            if(id){
                await fetch(`${process.env.REACT_APP_BACKEND_HOST}/reportCompletedQuest/${id}`,{
                    headers:{
                        Authorization:`Bearer ${token}`
                    }
                })

                // const json = await res.json();
                // console.log(json)
            }
        } catch (e) {
            // gracefully fail
            // dispatch(error())
        }
    }
}
export function AbandonQuest(id:string|undefined){
    return async (dispatch: Dispatch) => {
        try{
            const token = localStorage.getItem('token')
            // console.log(token)
            if(id){
                await fetch(`${process.env.REACT_APP_BACKEND_HOST}/abandonQuest/${id}`,{
                    headers:{
                        Authorization:`Bearer ${token}`
                    }
                })

                // const json = await res.json();
                // console.log(json)
            }
        } catch (e) {
            // gracefully fail
            // dispatch(error())
        }
    }
}

export function CancelQuest(id:string|undefined){
    return async (dispatch: Dispatch) => {
        try{
            const token = localStorage.getItem('token')
            // console.log(token)
            if(id){
                await fetch(`${process.env.REACT_APP_BACKEND_HOST}/cancelQuest/${id}`,{
                    headers:{
                        Authorization:`Bearer ${token}`
                    }
                })

                // const json = await res.json();
                // console.log(json)
            }
        } catch (e) {
            // gracefully fail
            // dispatch(error())
        }
    }
}

export type QuestBoardActions = ReturnType<typeof loadQuestBoard|typeof publishQuestSuccess>
