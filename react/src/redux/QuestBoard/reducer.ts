import {QuestBoardActions} from "./action"

export interface QuestBoard{
    id?:number;
    stage?:string;
    quest_name:string;
    user_id?:number;
    publish_date?:string;
    duration:string;
    expiration_date:string;
    prize:number;
    bail:number;
    difficulty:number|string;
    photo:File|null|undefined;
    about_quest:string;
    completed_by_user?:string;
    display_name?:string;
    username?:string;
    quest_id?:number;
    users_photo?: string;
    quest_photo?: string;
    took_by_user?: number;
}

export interface QuestBoardState{
    questBoard:QuestBoard[]
}

const initialState:QuestBoardState = {
    questBoard:[]
}

// Can use immer.js
export const questBoardReducer = (state: QuestBoardState = initialState, action: QuestBoardActions): QuestBoardState => {
    switch(action.type){
        case "@@complaints/Load_quest_board":
            return{
                ...state,
                questBoard:action.questBoard
            }
        case "@@quest/PUBLISH_QUEST":
                let newQuestBoard = state.questBoard.slice();
                newQuestBoard.push({
                    quest_name:action.questDetails.quest_name,
                    duration:action.questDetails.duration,
                    expiration_date:action.questDetails.expiration_date,
                    prize:parseInt(action.questDetails.prize),
                    bail:parseInt(action.questDetails.bail),
                    difficulty:parseInt(action.questDetails.difficulty),
                    photo:action.questDetails.photo,
                    about_quest:action.questDetails.about_quest
                })
                return{
                    ...state,
                    questBoard: newQuestBoard
                }
        default:
            return state
    }

    // }===  '@@complaints/LOAD_QUESTBOARD'){
	// 	return {
	// 		...state,
	// 		questBoard: action.questBoard
	// 	}
	// }
    // return state
}

export default questBoardReducer;