import {QuestBoardAcceptedQuestActions} from "./action"

export interface QuestBoardAcceptedQuest{
    id?:number;
    stage?:string;
    quest_name:string;
    user_id?:number;
    publish_date?:string;
    duration:string;
    expiration_date:string;
    prize:number;
    bail:number;
    difficulty:number|string;
    photo:File|null|undefined;
    about_quest:string;
    completed_by_user?:number;
    took_by_user?:number;
    username?:string;
    quest_id?:number;
    users_photo?: string;
    quest_photo?: string;
}

export interface QuestBoardAcceptedQuestState{
    questBoardAcceptedQuest:QuestBoardAcceptedQuest[]
}

const initialState:QuestBoardAcceptedQuestState = {
    questBoardAcceptedQuest:[]
}

export const questBoardAcceptedQuestReducer = (state: QuestBoardAcceptedQuestState = initialState, action: QuestBoardAcceptedQuestActions): QuestBoardAcceptedQuestState => {
    switch(action.type){
            case "@@complaints/LOAD_QUESTBOARD_ACCEPTED_QUEST":
                return{
                    ...state,
                    questBoardAcceptedQuest:action.questBoardAcceptedQuest
                }
        default:
            return state
    }

    // }===  '@@complaints/LOAD_QUESTBOARD'){
	// 	return {
	// 		...state,
	// 		questBoard: action.questBoard
	// 	}
	// }
    // return state
}

export default questBoardAcceptedQuestReducer;