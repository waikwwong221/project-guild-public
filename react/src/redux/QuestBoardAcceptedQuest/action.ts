import { Dispatch } from "redux";
// action creator
export function loadQuestBoardAcceptedQuest(questBoardAcceptedQuest: any) {
    return {
        type: '@@complaints/LOAD_QUESTBOARD_ACCEPTED_QUEST' as const,
        questBoardAcceptedQuest
    }
}

// thunk action creator
export function fetchQuestBoardAcceptedQuest() {
    return async (dispatch: Dispatch) => {
        try {
            const token = localStorage.getItem('token')
			// console.log(token)
            const res = await fetch(`${process.env.REACT_APP_BACKEND_HOST}/acceptedQuest`,{
                headers:{
                    Authorization:`Bearer ${token}`
                }
            })
            const json = await res.json();
            // console.log(json)
            dispatch(loadQuestBoardAcceptedQuest(json))
        } catch (e) {
            // gracefully fail
            // dispatch(error())
        }
    }
}


export type QuestBoardAcceptedQuestActions = ReturnType<typeof loadQuestBoardAcceptedQuest>
