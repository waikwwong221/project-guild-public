import { ChangeMenuActions } from "./action"

export interface MenuState {
    menu: boolean
}

const initialState: MenuState = {
    menu: true
}

export const menuReducer = (state: MenuState = initialState, action: ChangeMenuActions): MenuState => {
    if(action.type === "@@partyRooms/CHANGE_MENU") {
        const newMenu = action.menu
        return {
            ...state,
            menu: newMenu
        }
    }
    return state
}
