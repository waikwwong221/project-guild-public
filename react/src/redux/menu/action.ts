export function changeMenu(name: boolean) {
    return {
        type: '@@partyRooms/CHANGE_MENU' as const,
        menu: name,
    }
}

export type ChangeMenuActions = ReturnType<typeof changeMenu>