import { Dispatch } from "redux"

export function addCoins(id: string, coins: string){
    return async (dispatch: Dispatch) => {
        try{
            const token = localStorage.getItem('token')
            let addCoins = new FormData();
            addCoins.append("id", id)
            addCoins.append("coins", coins)
            // console.log("addCoins", addCoins)

            const res = await fetch(`${process.env.REACT_APP_BACKEND_HOST}/users/addUserMoney`,{
                method: "post",
                body: addCoins,
                headers:{
                    Authorization:`Bearer ${token}`
                }
            })

            await res.json();

        } catch (e) {
            // gracefully fail
            // dispatch(error())
        }
    }
}