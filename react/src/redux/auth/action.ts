// action creator

import { push } from "connected-react-router";
import { RootState, ThunkDispatch } from "../../store";
import { loginMessage } from "../LoginMessage/action";

export function loginSuccess(userId: number, token: string) {
    return {
        type: '@@auth/LOGIN_SUCCESS' as const,
        userId,
        token
    }
}

export function loginFailure() {
    return {
        type: '@@auth/LOGIN_FAILURE' as const,
    }
}

export function logoutSuccess() {
    return {
        type: '@@auth/LOGOUT_SUCCESS' as const,
    }
}

export type AuthActions =
    ReturnType<typeof loginSuccess> |
    ReturnType<typeof loginFailure> |
    ReturnType<typeof logoutSuccess> ;

// thunk action

export function login(username: string, password: string) {
    return async (dispatch: ThunkDispatch, getState: () => RootState) => {
        console.log(username)
        const res = await fetch(`${process.env.REACT_APP_BACKEND_HOST}/users/login`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                username, password
            })
        })
        const json = await res.json();
        // console.log(json)
        if (json.message === "User does not exist") {
            // Good 
            dispatch(loginMessage("User does not exist"))
        }

        if (json.message === "Password is incorrect") {
            dispatch(loginMessage("Password is incorrect"))
        }

        if (json.success) {
            localStorage.setItem('token',json.data)
            dispatch(loginSuccess(json.userId, json.data))
            // if (getState().router.location.pathname === '/login') {
                dispatch(push('/'))
            // }
        } else {
            dispatch(loginFailure())
        }
    }
}

export function checkLogin() {
    return async (dispatch: ThunkDispatch) => {
        const token = localStorage.getItem('token')
		// console.log(token)
        if (!token) {
			// console.log("a")
            dispatch(logoutSuccess())
            return;
        }

        const res = await fetch(`${process.env.REACT_APP_BACKEND_HOST}/users/currentUsers`, {
            headers: {
                'Authorization': `Bearer ${token}`
            }
        })
        const json = await res.json();
		// console.log(json)
        if (json.id) {
            dispatch(loginSuccess(json.id, token))
        } else {
            dispatch(logoutSuccess())
        }
    }
}


// export function fetchRegister(registerDate:Register) {
//     return async (dispatch: Dispatch) => {
//         try {
//             let register = new FormData();
//             register.append("username",registerDate.username)
//             register.append("password",registerDate.password)
//             register.append("email",registerDate.email)

//             const res = await fetch(`${process.env.REACT_APP_BACKEND_HOST}/users/register`,{
//                 method:"post",
//                 body:register,
//             })
//             const json = await res.json();
//             // if(json.success){
// 			// 	dispatch(registerSuccess(registerDate))
// 			// }
//         } catch (e) {
//             // gracefully fail
//             // dispatch(error())
//         }
//     }
// }

export function logout() {
    return (dispatch: ThunkDispatch) => {
        localStorage.removeItem('token')
        dispatch(logoutSuccess())
    }
}