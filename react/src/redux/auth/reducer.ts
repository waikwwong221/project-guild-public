import { AuthActions } from "./action"

export interface AuthState {
    isAuthenticated:boolean | null,
    userId: number,
    token: string
}

const initialState: AuthState = {
    isAuthenticated:localStorage.getItem("token") ? true : false,
    userId: 0,
    token: ''
}

export const authReducer = (state: AuthState = initialState, action: AuthActions): AuthState => {
    if (action.type === '@@auth/LOGIN_SUCCESS') {
        return {
            ...state,
            isAuthenticated: true,
            userId: action.userId,
            token: action.token
        }
    } else if (action.type === '@@auth/LOGIN_FAILURE') {
        return {
            ...state,
            isAuthenticated: false,
            userId: 0,
            token: ''
        }
    } else if (action.type === '@@auth/LOGOUT_SUCCESS') {
        return {
            ...state,
            isAuthenticated: false,
            userId: 0,
            token: ''
        }
    }
    return state
}