
// Can be used as more than just message for login
// Error message for any generic error
export function loginMessage(message: string) {
    return {
        type: '@@loginMessage/LOGIN_ERROR_MESSAGE' as const,
        errorMessage: message,
    }
}

export type LoginMessageActions = ReturnType<typeof loginMessage>