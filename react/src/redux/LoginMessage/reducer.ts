import { LoginMessageActions } from "./action"

export interface LoginMessageState {
    errorMessage: string
}

const initialState: LoginMessageState = {
    errorMessage: ""
}

export const LoginMessageReducer = (state: LoginMessageState = initialState, action: LoginMessageActions): LoginMessageState => {
    if(action.type === "@@loginMessage/LOGIN_ERROR_MESSAGE") {
        const newErrorMessage = action.errorMessage
        return {
            ...state,
            errorMessage: newErrorMessage
        }
    }
    return state
}
