import {IChatRoomActions,ResMessage} from "./actions";

export interface IChatRoomState{
	message:ResMessage[];
	receivedSuccess:boolean|null;
	sentSuccess:boolean|null
}

const initialState = {
	message:[],
	receivedSuccess:null,
	sentSuccess:null
}

const chatRoomReducer = (state:IChatRoomState = initialState,action:IChatRoomActions):IChatRoomState => {
	switch(action.type){
		case "@@chatRoom/Send_message":
			return{
				...state,
				sentSuccess:true
			}
		case "@@chatRoom/Get_message":
			return{
				...state,
				message:action.message,
				receivedSuccess:true
			}
		case "@@chatRoom/Update_send":
			return {
				...state,
				message:action.message
			}
		default:
			return state
	}
}

export default chatRoomReducer;