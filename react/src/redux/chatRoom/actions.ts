// import console from "console";
import { Dispatch } from "redux";

export interface ResMessage{
	sent_user:number;
	content:string;
	created_at:string;
}


export function getMessage(id: number) {
	return async (dispatch: Dispatch<IChatRoomActions>) => {
		const token = localStorage.getItem('token')
		const res = await fetch(`${process.env.REACT_APP_BACKEND_HOST}/chatroom/messages/${id}`,{
			headers:{
				Authorization:`Bearer ${token}`
			}
		})
		const result = await res.json()
		// console.log(result)
		if(result.success){
			dispatch(getMessageSuccess(result.message))
		}else{
			dispatch(failed("Get_message_failed",result.message))
		}
		// console.log(result)
	}
}
export function sendMessage(receiverId:number,message:string){
	return async (dispatch:Dispatch<IChatRoomActions>) => {
		const token = localStorage.getItem('token')
		// console.log(token)
		const res = await fetch(`${process.env.REACT_APP_BACKEND_HOST}/chatroom/messages`, {
			method: "post",
			headers: {
				Authorization: `Bearer ${token}`,
				"Content-type": "application/json"
			},
			body: JSON.stringify({ receiverId, message })
		})
		const result = await res.json();
		// console.log(result)
		if (result.success) {
			dispatch(sendMessageSuccess())
		} else {
			dispatch(failed("Get_thing_failed", result.message))
		}
	}
}

function sendMessageSuccess() {
	return {
		type: "@@chatRoom/Send_message" as const, // as const is important
	}
}
function getMessageSuccess(message:ResMessage[]){
	return{
		type:"@@chatRoom/Get_message" as const,
		message
	}
}

export function updateSend(message:ResMessage[]){
	return{
		type:"@@chatRoom/Update_send" as const,
		message
	}
}

type Failed = "Send_message_failed" | "Get_thing_failed"|"Get_message_failed"
function failed(type: Failed, message: string) {
	return {
		type,
		message
	}
}

export type IChatRoomActions = ReturnType<typeof sendMessageSuccess|typeof getMessageSuccess|typeof updateSend|typeof failed>
