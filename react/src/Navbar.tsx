import React, { useEffect, useState } from "react";
import * as FaIcons from "react-icons/fa"
import * as AiIcons from "react-icons/ai"
import * as GiIcons from "react-icons/gi"
import * as IoLogOutOutline from 'react-icons/io5'
import { Link, NavLink } from 'react-router-dom'
import { SidebarData } from './NavbarData'
import './Navbar.css';
import { IconContext } from 'react-icons';
import { useDispatch, useSelector } from 'react-redux';
import { fetchUserData } from "./redux/UserData/action";
import { RootState } from "./store";
import { logout } from "./redux/auth/action";
import coins from './photo/gold_coin.png'
import girl from "./photo/guild (9).png"

function Navbar() {
	const dispatch = useDispatch();
	useEffect(() => {
		dispatch(fetchUserData());
	}, [dispatch]);
	const userData = useSelector((state: RootState) => state.userData.userData);
	const userId = useSelector((state: RootState) => state.auth.userId);
	// const newUserDate = userData.filter(c => c.id === userId)
	// console.log(userId)

	const [sidebar, setSidebar] = useState(false);
	const toggleSideBar = () => setSidebar(!sidebar);

	if(userData && userData.length > 0){
		for (let c of userData) {
			if (c.display_name === null) {
				c.display_name = `User ${c.id}`
			}
		}
	}

	return (
		<>
			<div className="outside">
				<IconContext.Provider value={{ color: '#fff' }}>
					{userData && userData.length > 0 &&  userData.filter(c => c.id === userId).map((c, index) => {
						return (
							<div key={index} className="navbar">
								<NavLink to="#" className="menu-bar">
									<FaIcons.FaBars onClick={toggleSideBar} />
								</NavLink>
								<div className="Container">
									<div className="left-Container">
										<div className="user_Name">{c.display_name}</div>
										<div className="questIcons">
											<GiIcons.GiRolledCloth className="roll"/>
										</div>
										<div className="questCount">{c.taking_quest}</div>
									</div>
									<div className="right-Container">
										<div ><img src={coins} className="coins" alt=""/></div>
										<div className="userCurrency">${c.currency}</div>
										<div className="bell">
											<GiIcons.GiRingingBell />
										</div>
									</div>
								</div>
							</div>
						)
					})}
					<nav className={sidebar ? 'nav-menu active' : 'nav-menu'}>
						<ul className='nav-menu-item'>
							<li className='navbar-toggle'>
								<Link to='#' className='menu-bars' onClick={toggleSideBar}>
									<AiIcons.AiOutlineClose />
								</Link>
							</li>
							{SidebarData.map((item, index) => {
								return (
									<li key={index} className={item.cName}>
										<Link to={item.path} onClick={toggleSideBar}>
											{item.icon}
											<span>{item.title}</span>
										</Link>
									</li>
								);
							})}
							<li className={"nav-text"}>
								<Link onClick={() => {
									dispatch(logout())
								}} to={"/"}>
									{<IoLogOutOutline.IoLogOutOutline />}
									<span>{"Logout"}</span>
								</Link>
							</li>
							<div className="Navber-girl-Display">
								<img src={girl} alt=""></img>
							</div>
						</ul>
					</nav>
				</IconContext.Provider>
			</div>
		</>
	)
}

export default Navbar;