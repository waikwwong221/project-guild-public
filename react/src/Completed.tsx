import React, { useEffect } from 'react'
import { NavLink, useRouteMatch } from 'react-router-dom'
import './Completed.css'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faArrowLeft } from '@fortawesome/free-solid-svg-icons'
import { useDispatch, useSelector } from 'react-redux'
import { fetchUserData } from './redux/UserData/action'
import { fetchPublishedQuest } from './redux/QuestBoard/action'
import { RootState } from './store'
import { fetchQuestBoardAcceptedQuest } from './redux/QuestBoardAcceptedQuest/action'
import completed from './photo/completed.png'
import Finished from './photo/Finished.png'
import Published from './photo/Published.png'

export function Completed() {
    const dispatch = useDispatch();
    useEffect(() => {
        dispatch(fetchPublishedQuest())
        dispatch(fetchUserData())
        dispatch(fetchQuestBoardAcceptedQuest())
    }, [dispatch])
    const QuestBoard = useSelector((state: RootState) => state.questBoard.questBoard)
    const router = useRouteMatch<{id?: string}>()
    const currentQuestBoard = QuestBoard.find(c => c.quest_id === parseInt(router.params.id ?? ''))
    const acceptedUser = QuestBoard.find(c => c.user_id === currentQuestBoard?.took_by_user)

    for(let c of QuestBoard){
        if (c.difficulty === 1) {
            c.difficulty = "★☆☆☆☆"
        } else if (c.difficulty === 2) {
            c.difficulty = "★★☆☆☆"
        } else if (c.difficulty === 3) {
            c.difficulty = "★★★☆☆"
        } else if (c.difficulty === 4) {
            c.difficulty = "★★★★☆"
        } else if (c.difficulty === 5) {
            c.difficulty = "★★★★★"
        }
        if (c.bail === null) {
            c.bail = 0
        }
        if (c.users_photo === null) {
            c.users_photo = "nullUserIcon.png"
        }
        if (c.display_name === null) {
            c.display_name = `User ${c.id}`
        }
    }

    return (
        <div className="Completed-Display">
            <div className="UserPage-BackMain">
                <NavLink to="/main" style={{textDecoration: 'none'}}>
                    <div className="UserPage-Main-Button">
                        <div className="UserPage-Main-Button-fa"><FontAwesomeIcon icon={faArrowLeft} /></div>
                        <div className="UserPage-Main-Button-Title">Main Menu</div>
                    </div>
                </NavLink>
            </div>
            {currentQuestBoard &&
                <div>
                    <div className="Completed-Title">Comfirm the Quest Result</div>
                    <div className="Completed-Like-Title">Leave a ‘like’ if you think he/she done well!</div>
                    <div className="Completed-User-Display">
                        <div className="Completed-User-Icon">
                        <img src={`${process.env.REACT_APP_BACKEND_HOST}/${acceptedUser?.photo}`} alt=""></img>
                        </div>
                        <div>
                            <div className="Completed-User-UserName">{acceptedUser?.display_name}</div>
                            <div className="Completed-User-Star">{acceptedUser?.difficulty}</div>
                        </div>
                    </div>
                </div>
            }
            <div className="Completed-Img">
                <img className="Img-Completed" src={completed} alt=""></img>
                <img className="Img-Finished" src={Finished} alt=""></img>
                <img className="Img-Published" src={Published} alt=""></img>
            </div>
        </div>
    )
}