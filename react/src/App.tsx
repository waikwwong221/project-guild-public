import React,{useEffect} from 'react';
import {useDispatch,useSelector} from 'react-redux';
import {Route,Switch} from 'react-router';
import './App.css';
import {BottomBar} from './BottomBar';
import {FrontPage} from './FrontPage';
import {Login} from './Login';
import {Main} from './Main';
import {PublishQuest} from './PublishQuest';
import {QuestBoard} from './QuestBoard';
import {QuestBoardDetail} from './QuestBoardDetail';
import {RootState} from './store';
import Navbar from './Navbar';
import {checkLogin} from './redux/auth/action';
import {Register} from './Register';
import {UserPage} from './UserPage';
import {Setting} from './Setting';
import {ChatRoom} from './ChatRoom';
import {PrivateChatRoom} from './PrivateChatRoom';
import { Finish } from './Finish';
import { Completed } from './Completed';
import CropPhoto from './CropPhoto'
import { MyCard } from './MyCard';

function App() {
	const isAuthenticated = useSelector((state: RootState) => state.auth.isAuthenticated);
	const dispatch = useDispatch();
	// const menu = useSelector((state: RootState) => state.menu.menu)
	// const [Menu, setMenu] = useState<boolean>(menu)
	useEffect(() => {
		dispatch(checkLogin());
	},[dispatch]);
	

	// For wrong url, better to redirect to frontpage to maintain a correct url 
	// url normally use hyphen


	// 1. Listing 
	// 2. Detail
	// 3. Create/ Edit Form (Update, Create  + Delete )
	return (
		<div className="App">
			{!isAuthenticated &&
				<Switch>
					<Route exact path="/register"><Register /></Route>
					<Route path="/"><Login /></Route>
				</Switch>
			}
			{isAuthenticated &&
				<div>
					<div className="App-HeaderBar">
						<Navbar />
					</div>
					<div className="App-Content" >
						<Switch>
							<Route exact path="/main">
								<Main />
								<BottomBar />
							</Route>
							<Route exact path="/userPage"><UserPage /></Route>
							<Route exact path="/Setting"><Setting /></Route>
							<Route exact path="/publishQuest"><PublishQuest /></Route>
							<Route exact path="/questBoard"><QuestBoard /></Route>
							<Route exact path="/questBoard/:id"><QuestBoardDetail /></Route>
							<Route exact path="/chatroom"><ChatRoom /></Route>
							<Route exact path="/chatroom/:id"><PrivateChatRoom /></Route>
							<Route exact path="/finishedQuest/:id"><Finish /></Route>
							<Route exact path="/completed/:id"><Completed /></Route>
							<Route exact path="/myCard"><MyCard /></Route>
							<Route exact path="/CropPhoto"><CropPhoto /></Route>
							<Route path="/"><FrontPage /></Route> 
						</Switch>
					</div>
				</div>
			}
		</div>
	);
}

export default App;
