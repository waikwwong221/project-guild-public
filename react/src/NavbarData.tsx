// import * as FaIcons from 'react-icons/fa'
// import * as AiIcons from 'react-icons/ai'
// import * as IoIcons from 'react-icons/io'
import * as GiIcons from 'react-icons/gi';


export const SidebarData = [
    {
        title:'Quest',
        path:'/',
        icon: <GiIcons.GiScrollQuill />,
        cName: 'nav-text'
    },
    {
        title:'user',
        path:'/userPage',
        icon: <GiIcons.GiBeamsAura />,
        cName: 'nav-text'
    },
    {
        title:'Chat',
        path:'/chatroom',
        icon: <GiIcons.GiCampfire />,
        cName: 'nav-text'
    },
    {
        title:'QuestBoard',
        path:'/QuestBoard',
        icon: <GiIcons.GiScrollUnfurled />,
        cName: 'nav-text'
    },
    {
        title:'Setting',
        path:'/Setting',
        icon: <GiIcons.GiSettingsKnobs />,
        cName: 'nav-text'
    },
    {
        title:'About_us',
        path:'/About_us',
        icon: <GiIcons.GiInfo />,
        cName: 'nav-text'
    }
]