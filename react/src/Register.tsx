import { useState } from 'react';
import { useDispatch } from 'react-redux';
import { fetchRegister } from './redux/register/action';
import './Register.css'
import {push} from 'connected-react-router'

export function Register() {
    const dispatch = useDispatch();
    const [username, setUsername] = useState("")
    const [email, setEmail] = useState("")
    const [password, setPassword] = useState("")
    const [ConfirmPassword, setConfirmPassword] = useState("")
    const [errorMessage,setErrorMessage] = useState('')

    return (
        <div className="Register-Display">
            <div className="Register-Title">Sign Up</div>
            <form onSubmit={(event) => {
                event.preventDefault();
                if (password !== ConfirmPassword) {
                    setErrorMessage(errorMessage => errorMessage = "password not match")
                    return
                }
                dispatch(fetchRegister({username, email, password}))
				alert("已成功創建新用戶！")
				dispatch(push("/"))
            }}>
                <div className="Register-UserDate">
                    <input type="text" value={username} required placeholder="Username" onChange={(event) => {
                        setUsername(event.currentTarget.value)
                    }}></input>
                    <input type="email" value={email} required placeholder="Email" onChange={(event) => {
                        setEmail(event.currentTarget.value)
                    }}></input>
                    <input type="password" value={password} required placeholder="Password" onChange={(event) => {
                        setPassword(event.currentTarget.value)
                    }}></input>
                    <input type="password" value={ConfirmPassword} required placeholder="Confirm Password" onChange={(event) => {
                        setConfirmPassword(event.currentTarget.value)
                    }}></input>
                    <div className="Register-Error-Message">{errorMessage}</div>
                    <div className="Register-Button">
                        <input className="Register-Button-Title" type="submit" value="Register my account"/>
                    </div>
                    <div className="Register-Conditions">By registering you agree to our Terms & Conditions.</div>
                </div>
            </form>
        </div>
    )
}