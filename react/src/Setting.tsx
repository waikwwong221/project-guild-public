import { push } from 'connected-react-router';
import React from 'react';
import { useDispatch } from 'react-redux';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faArrowLeft } from '@fortawesome/free-solid-svg-icons'
import './Setting.css';
import { NavLink } from 'react-router-dom';

export function Setting() {
    const dispatch = useDispatch()

    return (
        <div className="Setting-Display">
            <div className="UserPage-BackMain">
                <NavLink to="/main" style={{textDecoration: 'none'}}>
                    <div className="UserPage-Main-Button">
                        <div className="UserPage-Main-Button-fa"><FontAwesomeIcon icon={faArrowLeft} /></div>
                        <div className="UserPage-Main-Button-Title">Main Menu</div>
                    </div>
                </NavLink>
            </div>
            <div className="Setting-Deposit">
                <div className="Setting-Deposit" onClick={() => {
                    dispatch(push('/myCard'))
                }}>Click me Deposit!</div>
            </div>
            <hr></hr>
            <div className="abc">
                <div className="Language">Language</div>
                <div className="Show_userLanguage">Eng</div>
                <div className="Change">Change</div>
            </div>
        </div>
    )
}