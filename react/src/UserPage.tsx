import { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { fetchUserData, userDataUpdate } from './redux/UserData/action'
import { RootState } from './store'
import './UserPage.css'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faArrowLeft } from '@fortawesome/free-solid-svg-icons'
import { faUserCog, faPlus, faCheckCircle} from '@fortawesome/free-solid-svg-icons'
import { NavLink } from 'react-router-dom'
import ribbon from './photo/ribbon.png'
import { fetchQuestBoardAcceptedQuest } from './redux/QuestBoardAcceptedQuest/action'
import { fetchPublishedQuest } from './redux/QuestBoard/action'
import { push } from 'connected-react-router'
import React from 'react'
// import AvatarEditor from 'react-avatar-editor'
// import CropPhoto from './CropPhoto'

export function UserPage() {
    const dispatch = useDispatch();

    // Obviously too much data
    useEffect(() => {
        dispatch(fetchPublishedQuest())
        dispatch(fetchUserData())
        dispatch(fetchQuestBoardAcceptedQuest())
    }, [dispatch])
    const questBoard = useSelector((state: RootState) => state.questBoard.questBoard);
    const userData = useSelector((state: RootState) => state.userData.userData)
    const userId = useSelector((state: RootState) => state.auth.userId)
    // Should be done in backend
    const newUserData = userData.filter(c => c.id === userId)
    // Should be done in backend
    const recentActivities = questBoard.filter(c => c.user_id === userId || c.took_by_user === userId)

    const oldDisplay_name = newUserData.map(c => c.display_name).toString()
    const oldSelf_intro = newUserData.map(c => c.self_intro).toString()
    const [setting, setSetting] = useState<Boolean>(false)
    // Display name and self intro will be null if you refresh in this page becoz useEffect is not done yet.
    // Advise to use a separate page for editing
	const [display_name, setDisplay_name] = useState(oldDisplay_name);
	const [self_intro, setSelf_intro] = useState(oldSelf_intro);
    const [photo,setPhoto] = useState<File|null>();

    for (let c of userData) {
        if (c.rank === 1) {
            c.rank = "★☆☆☆☆"
        } else if (c.rank === 2) {
            c.rank = "★★☆☆☆"
        } else if (c.rank === 3) {
            c.rank = "★★★☆☆"
        } else if (c.rank === 4) {
            c.rank = "★★★★☆"
        } else if (c.rank === 5) {
            c.rank = "★★★★★"
        } else if (c.rank === 0) {
            c.rank = "☆☆☆☆☆"
        }
        if (c.photo === null) {
            c.photo = "nullUserIcon.png"
        }
        if (c.self_intro === null) {
            c.self_intro = "( 未有新增個人信息 )"
        }
        if (c.display_name === null) {
            c.display_name = `User ${c.id}`
        }
    }

    return (
        <div className="UserPage-Display">
            <div className="UserPage-BackMain">
                <NavLink to="/main" style={{textDecoration: 'none'}}>
                    <div className="UserPage-Main-Button">
                        <div className="UserPage-Main-Button-fa"><FontAwesomeIcon icon={faArrowLeft} /></div>
                        <div className="UserPage-Main-Button-Title">Main Menu</div>
                    </div>
                </NavLink>
            </div>
        <form onSubmit={(event) => {
            // Better into separate function;
            event.preventDefault();
            dispatch(userDataUpdate(display_name, self_intro, photo, userId))
            setSetting(setting => false)
            alert("完成修改檔案") // Avoid using alert, confirm, info since it blocks the website
            dispatch(fetchUserData())
        }}>
        <div className="UserPage-Content-Display">
            <div className="UserPage-Content-Setting-Display">
                <div className="UserPage-User-Title">User</div>
                {setting ?
                    <label>
                        <div className="UserPage-User-submit-Button">
                            <FontAwesomeIcon icon={faCheckCircle} />Finish edit!
                        </div>
					    <input className="UserPage-User-submit" type="submit" value="完成修改檔案" />
                    </label>:
                    <div className="UserPage-Content-Setting" onClick={() => [
                        setSetting(setting => true),
                        alert("修改個人檔案")
                    ]}><FontAwesomeIcon icon={faUserCog} />Edit Profile</div>
                }
            </div>
            {newUserData.map((c,index) => {
                return(
                <div key={index} className="UserPage-UserDate-Display">
                    <div className="UserPage-UserDate-Content">
                        <div className="UserPage-UserIcon-Display">
                            {setting ?
                                <label>
                                    <div className="UserPage-UserIcon-Button">
                                        <FontAwesomeIcon icon={faPlus} />
                                    </div>
                                    <input className="UserPage-UserIcon-file" type="file" accept="image/jpeg, image/png," onChange={(event) => {
                                        const files = event.currentTarget.files;
                                        if(files){
                                            setPhoto(files[0])
                                        }
                                    }}/>
                                </label> :
                                <img src={`${process.env.REACT_APP_BACKEND_HOST}/${c.photo}`} alt=""></img>
                            }
                        </div>
                        <div className="UserPage-UserDate">
                            {setting ?
                                <input type="text" value={display_name} required onChange={(event) => {
                                    // Use react-hook-form
                                    setDisplay_name(event.currentTarget.value);
                                }}/> :
                                <div className="UserPage-User-username">Display Name: {c.display_name}</div>
                            }
                            <div className="UserPage-Rank-Display">
                                <div className="UserPage-User-Rank">Rank</div>
                                <div className="UserPage-User-Rank-Color">{c.rank}</div>
                            </div>
                            <div className="UserPage-User-star-ID">
                                <div className="UserPage-User-star">
                                    <img src={ribbon} alt=""></img>
                                </div>
                                <div className="UserPage-User-ID">ID:{c.id}</div>
                            </div>
                        </div>
                    </div>
                    <div className="UserPage-UserDate-Bottom">
                        <div className="UserPage-UserDate-Balance-JoinDate">
                            <div className="UserPage-UserDate-Balance">BALANCE: ${c.currency}</div>
                            <div className="UserPage-UserDate-JoinDate">Join Date: {c.created_at.substr(0, 10)}</div>
                        </div>
                        <div className="UserPage-UserDate-Message-Display">
                            <div className="UserPage-UserDate-QuestNumber">
                            <div className="UserPage-UserDate-Exp">Num of Completed Quest: {questBoard.filter(quest=>quest.user_id === userId && quest.stage === "completed").length}</div>
                                <div className="UserPage-UserDate-published-quest">Num of Published Quest: {questBoard.filter(quest=>quest.user_id === userId).length}</div>
                            </div>
                            <div className="UserPage-UserDate-Message" onClick={() => {

                            }}>
                                {setting ?
                                    <textarea value={self_intro} rows={5} cols={25} onChange={(event) => {
                                        setSelf_intro(event.currentTarget.value)
                                    }}></textarea> :
                                    <div className="UserPage-UserDate-Message-Content">
                                    {c.self_intro}</div>
                                }
                            </div>
                        </div>
                    </div>
                </div>
            )})}
            <div className="UserPage-Recent-Activities">Recent Activities</div>
            <div className="UserPage-Recent-Activities-Display">
                {recentActivities.map((c, index) => (
					<div key={index}>
                    {c.user_id === userId && c.stage === "completed" &&
					<div className="UserPage-Recent-Activities-Data" onClick={() => [
						dispatch(push('/questBoard/' + c.quest_id))
					]}>
						<div className="UserPage-Recent-Activities-Data-username">
							<div>{c.quest_name}</div>
							<div>Id: {c.quest_id}</div>
						</div>
						<div className="UserPage-Recent-Activities-Data-Date">
							<div className="UserPage-Completed-Display">
								<div className="UserPage-Completed-boll"></div>
								<div className="UserPage-Completed-Title">Completed</div>
							</div>
							<div>{c.publish_date?.substr(0, 10)}</div>
						</div>
						<div className="UserPage-Recent-Activities-Data-Published-Money">
							<div>${c.prize}</div>
						</div>
					</div>}
                    {c.took_by_user === userId && c.stage === "completed" &&
					<div className="UserPage-Recent-Activities-Data" onClick={() => [
						dispatch(push('/questBoard/' + c.quest_id))
					]}>
						<div className="UserPage-Recent-Activities-Data-username">
							<div>{c.quest_name}</div>
							<div>Id: {c.quest_id}</div>
						</div>
						<div className="UserPage-Recent-Activities-Data-Date">
							<div className="UserPage-Finished-Display">
								<div className="UserPage-Finished-boll"></div>
								<div className="UserPage-Finished-Title">Finished</div>
							</div>
							<div>{c.publish_date?.substr(0, 10)}</div>
						</div>
						<div className="UserPage-Recent-Activities-Data-Accepted-Money">
							<div>${c.prize}</div>
						</div>
					</div>}
                    {c.user_id === userId && c.stage !== "completed" &&
					<div className="UserPage-Recent-Activities-Data" onClick={() => {
                        // Can be replaced with <Link>
						dispatch(push('/questBoard/' + c.quest_id))
                    }}>
						<div className="UserPage-Recent-Activities-Data-username">
							<div>{c.quest_name}</div>
							<div>Id: {c.quest_id}</div>
						</div>
						<div className="UserPage-Recent-Activities-Data-Date">
							<div className="UserPage-Published-Display">
								<div className="UserPage-Published-boll"></div>
								<div className="UserPage-Published-Title">Published</div>
							</div>
							<div>{c.publish_date?.substr(0, 10)}</div>
						</div>
						<div className="UserPage-Recent-Activities-Data-Published-Money">
							<div>${c.prize}</div>
						</div>
					</div>}
				  	{c.took_by_user === userId && c.stage !== "completed" &&
					<div className="UserPage-Recent-Activities-Data" onClick={() => [
						dispatch(push('/questBoard/' + c.quest_id))
					]}>
						<div className="UserPage-Recent-Activities-Data-username">
							<div>{c.quest_name}</div>
							<div>Id: {c.quest_id}</div>
						</div>
						<div className="UserPage-Recent-Activities-Data-Date">
							<div className="UserPage-Accepted-Display">
								<div className="UserPage-Accepted-boll"></div>
								<div className="UserPage-Accepted-Title">Accepted</div>
							</div>
							<div>{c.publish_date?.substr(0, 10)}</div>
						</div>
						<div className="UserPage-Recent-Activities-Data-Accepted-Money">
							<div>${c.prize}</div>
						</div>
					</div>}
					</div>
                ))}
            </div>
        </div>
        </form>

        </div>
    )
}