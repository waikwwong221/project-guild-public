import {useState} from "react";
import {useDispatch,useSelector} from "react-redux";
import {login, loginSuccess} from "./redux/auth/action";
import './Login.css';
import guild from './photo/guild.png';
// import facebook from './photo/facebookIcon.png';
// import google from './photo/googleIcon.png';
import {NavLink} from "react-router-dom";
import {push} from "connected-react-router";
import {RootState} from "./store";
import ReactFacebookLogin,{ReactFacebookLoginInfo} from "react-facebook-login";

export function Login(){
    const errorMessage = useSelector((state: RootState) => state.loginMessage.errorMessage)
    const [username, setUsername] = useState('')
    const [password, setPassword] = useState('')
    const dispatch = useDispatch();

    return(
        <div className="Login-Display">
            <div className="Login-Guild-Icon">
                <img src={guild} alt=""></img>
            </div>
            <form onSubmit={e => {
                e.preventDefault();
                dispatch(login(username, password))
                dispatch(push("/"))
            }}>
                <div className="Login-Guild-Login">
                    <input className="Login-Guild-Login-username" required placeholder="username" value={username} onChange={e => setUsername(e.currentTarget.value)}/>
                    <input className="Login-Guild-Login-password" required placeholder="password" type="password" value={password} onChange={e => setPassword(e.currentTarget.value)}/>
                    <div className="Login-Guild-Error-Message">{errorMessage}</div>
                    <div className="Login-Guild-Button">
                        <div className="Login-Guild-Button-checkbox">
                            <input type="checkbox"></input>
                            <div className="Login-Guild-RememberMe">Remember me</div>
                        </div>
                        <div className="Login-Guild-Forgot">Forgot password?</div>
                    </div>
                    <input className="Login-Guild-Login-Submit" type="submit" value="Login"/>
                    <NavLink exact to="/register">
                    <div className="Login-Register-button">
                        <div className="Login-Register-button-Title">Register</div>
                    </div>
                    </NavLink>
                </div>
            </form>
			<ReactFacebookLogin
				appId={process.env.REACT_APP_FACEBOOK_APP_ID || ""}
				autoLoad={false}
				fields="name,email,picture,birthday"
				authType="rerequest"
				callback={async(user:ReactFacebookLoginInfo) => {
					const {accessToken} = user
					const res = await fetch(`${process.env.REACT_APP_BACKEND_HOST}/users/facebookLogin`,{
						method:"post",
						headers:{
							"Content-Type":"application/json"
						},
						body:JSON.stringify({accessToken})
					})
					const result = await res.json();
					// console.log(result)
					if(result.success){
						localStorage.setItem('token',result.data)
						dispatch(loginSuccess(result.id,result.data))
					}
				}}
			/>
            {/* <button className="Login-Guild-Facebook-Login" onClick={() => {
                window.location.href = 'http://facebook.com/v10.0/dialog/oauth?client_id=565770491057482&redirect_uri=http://localhost:3000/facebook/callback'
            }}><img src={facebook} alt=""></img>Log In with Facebook</button> */}
            {/* <button className="Login-Guild-Google-Login" onClick={() => {

            }}><img src={google} alt=""></img>Log In with Google</button> */}
        </div>
    )
}