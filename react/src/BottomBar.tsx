// import react from 'react'
import task from './photo/main.png'
import guild from './photo/guild.png'
import kuma from './photo/黑白熊.png'
import setting from './photo/setting.png'
import './BottomBar.css'
import { NavLink } from 'react-router-dom'
import Chat_icon from './photo/Chat_icon.png'
import Exclamation_mark from './photo/Exclamation_mark.png'
import { Notice } from './Notice'

export function BottomBar() {
    return (
        <div className="BottomBar-Display">
            <NavLink to="/questBoard" style={{textDecoration: 'none'}}>
                <div className="BottomBar-Display-SetBox">
                    <div className="BottomBar-Button box"><img src={guild} alt="hi"/></div>
                    <div>Missions</div>
                </div>
            </NavLink>
            <NavLink to="/userPage" style={{textDecoration: 'none'}}>
                <div className="BottomBar-Display-SetBox">
                    <div className="BottomBar-Button"><img src={kuma} alt="hi"/></div>
                    <div>User</div>
                </div>
            </NavLink>
            <NavLink to="/chatroom" style={{textDecoration: 'none'}}>
                <div className="BottomBar-Display-SetBox">
                    <div className="BottomBar-Button"><img src={Chat_icon} alt="hi"/></div>
                    <div>contact</div>
                </div>
            </NavLink>
            <NavLink to="/main" style={{textDecoration: 'none'}} onClick={() => {
                <Notice></Notice>
            }}>
                <div className="BottomBar-Display-SetBox">
                    <div className="BottomBar-Button box2"><img src={Exclamation_mark} alt="hi"/></div>
                    <div>Notice</div>
                </div>
            </NavLink>
            <NavLink to="/Setting" style={{textDecoration: 'none'}}>
                <div className="BottomBar-Display-SetBox">
                    <div className="BottomBar-Button"><img src={setting} alt="hi"/></div>
                    <div>Setting</div>
                </div>
            </NavLink>
        </div>
    )
}
