import React from 'react'
import './PrivateBar.css'

export function PrivateBar() {
    return (
        <div className="PrivateBar-Display">
            <div className="PrivateBar-UserContent">
                <div className="PrivateBar-UserName">LouisKwok</div>
                <div className="PrivateBar-UserIcon">0////0</div>
            </div>
        </div>
    )
}