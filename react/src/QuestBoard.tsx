import './QuestBoard.css'
import main from './photo/guild (10).png'
import {useDispatch,useSelector} from 'react-redux'
import {RootState} from './store'
import {push} from 'connected-react-router'
import {useEffect} from 'react'
import {fetchQuestBoard} from "./redux/QuestBoard/action"
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faArrowLeft } from '@fortawesome/free-solid-svg-icons'
import { NavLink } from 'react-router-dom'

export function QuestBoard(){
    const dispatch = useDispatch()
    // Can use custom hooks
    useEffect(() => {
        dispatch(fetchQuestBoard())
    },[dispatch])
    const QuestBoard = useSelector((state: RootState) => state.questBoard.questBoard)


	for(let c of QuestBoard){
		if(c.difficulty === 1){
			c.difficulty = "★☆☆☆☆"
			}else if(c.difficulty === 2){
			c.difficulty = "★★☆☆☆"
			}else if(c.difficulty === 3){
			c.difficulty = "★★★☆☆"
			}else if(c.difficulty === 4){
			c.difficulty = "★★★★☆"
			}else if(c.difficulty === 5){
			c.difficulty = "★★★★★"
		}
        if(c.publish_date == null){
			c.publish_date = "no date"
		}
	}

    return (
        <div className="QuestBoard-Display">
            <div className="QuestBoard-Main-Search">
                <div className="QuestBoard-BackMain">
                    <NavLink to="/main" style={{textDecoration: 'none'}}>
                        <div className="QuestBoard-Main-Button">
                            <div className="QuestBoard-Main-Button-fa"><FontAwesomeIcon icon={faArrowLeft} /></div>
                            <div className="QuestBoard-Main-Button-Title">Main Menu</div>
                        </div>
                    </NavLink>
                    <NavLink to="/publishQuest" style={{textDecoration: 'none', color: "#000000"}}>
                    <div className="QuestBoard-PublishQuest">
                        <img src={main} alt="hi"></img>
                        <div className="Publish">Publish Quest</div>
                    </div>
                    </NavLink>
                </div>
                <div className="QuestBoard-Search">
                    <input type="text" placeholder="search"></input>
                </div>
            </div>
            <div className="QuestBoard-Guild-List">
                {QuestBoard.map((c,index) => {
                    // console.log(c.quest_photo)
                    return(
                    <div key={index} className="QuestBoard-card">
                        <div className="QuestBoard-card-Title">
                            <div className="QuestBoard-card-Title-Name">{c.quest_name.substr(0, 10) + "..."}</div>
                            <div className="QuestBoard-card-star-day">
                                <div className="QuestBoard-card-star">{c.difficulty}</div>
                                <div className="QuestBoard-card-day">{c.publish_date?.substr(0, 10)}</div>
                            </div>
                        </div>
                        <div className="QuestBoard-card-Content-Display">
                            <div className="QuestBoard-card-Content">{c.about_quest.substr(0, 20) + "..."}</div>
                            {c.quest_photo && <img src={`${process.env.REACT_APP_BACKEND_HOST}/${c.quest_photo}`} alt=""></img>}
                        </div>
                        <div className="QuestBoard-Button-Display">
                            <div className="QuestBoard-Button-Details">
                                <div className="QuestBoard-Button-Details-Color" onClick={() => {
                                    dispatch(push('/questBoard/' + c.quest_id))
                                }}>Details</div>
                            </div>
                            <div className="QuestBoard-card-Money">Prize: ${c.prize}</div>
                        </div>
                    </div>
                )})}
            </div>
        </div>
    )
}
