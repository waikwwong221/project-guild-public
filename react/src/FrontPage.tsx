import FrontPageIcon from "./photo/book(2).png";
import hand_up from "./photo/hand_up.png";
import "./FrontPage.css";
import { NavLink } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "./store";
import { useEffect } from "react";
import { fetchPublishedQuest } from "./redux/QuestBoard/action";
import { fetchQuestBoardAcceptedQuest } from "./redux/QuestBoardAcceptedQuest/action";
import { push } from "connected-react-router";
import Carousel from "nuka-carousel";

export function FrontPage() {
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(fetchPublishedQuest());
    dispatch(fetchQuestBoardAcceptedQuest());
  }, [dispatch]);

  const userId = useSelector((state: RootState) => state.auth.userId);
  const questBoard = useSelector(
    (state: RootState) => state.questBoard.questBoard
  );
  const questBoardAcceptedQuest = useSelector(
    (state: RootState) => state.questBoardAcceptedState.questBoardAcceptedQuest
  );
  const LoginUserAcceptedQuest = questBoardAcceptedQuest.filter(
    (c) => c.took_by_user === userId && c.stage !== "completed"
  );
  const LoginUserPublishedQuest = questBoard.filter(
    (c) =>
      c.user_id === userId && c.stage !== "completed" && c.stage !== "cancelled"
  );

  for (let c of questBoard) {
    if (c.difficulty === 1) {
      c.difficulty = "★☆☆☆☆";
    } else if (c.difficulty === 2) {
      c.difficulty = "★★☆☆☆";
    } else if (c.difficulty === 3) {
      c.difficulty = "★★★☆☆";
    } else if (c.difficulty === 4) {
      c.difficulty = "★★★★☆";
    } else if (c.difficulty === 5) {
      c.difficulty = "★★★★★";
    }
  }
  for (let c of questBoardAcceptedQuest) {
    if (c.difficulty === 1) {
      c.difficulty = "★☆☆☆☆";
    } else if (c.difficulty === 2) {
      c.difficulty = "★★☆☆☆";
    } else if (c.difficulty === 3) {
      c.difficulty = "★★★☆☆";
    } else if (c.difficulty === 4) {
      c.difficulty = "★★★★☆";
    } else if (c.difficulty === 5) {
      c.difficulty = "★★★★★";
    }
  }

  return (
    <div className="FrontPage-Display">
      {LoginUserAcceptedQuest.length > 0 && (
        <Carousel>
          {LoginUserAcceptedQuest.map((c, index) => {
            return (
              <div key={index} className="Main-Accepted-Quests-Display">
                <div className="Main-Accepted-Quests-Title">
                  Accepted Quests
                </div>
                <div className="Main-Accepted-Quests-Quest-Display">
                  <div className="Main-Accepted-Quests-Quest-Display-Box">
                    <div>
                      <div className="Main-Accepted-Quests-Star-Display">
                        <div className="Main-Accepted-Quests-Quest-Title">
                          {c.quest_name.substr(0, 10) + "..."}
                        </div>
                        <div className="Main-Accepted-Quests-Quest-Star">
                          {c.difficulty}
                        </div>
                      </div>
                      <div className="Main-Accepted-Quests-Quest-Content">
                        <div>{c.about_quest.substr(0, 20) + "..."}</div>
                      </div>
                    </div>
                    <div className="Main-Accepted-Quests-Quest-Details-Display">
                      <div className="Main-Accepted-Quests-Quest-Prize">
                        PRIZE: ${c.prize}
                      </div>
                      <div
                        className="Main-Accepted-Quests-Quest-Details"
                        onClick={() => {
                          dispatch(push("/questBoard/" + c.quest_id));
                        }}
                      >
                        Details
                      </div>
                    </div>
                  </div>
                  <div className="Main-Accepted-Quests-Quest-Photo">
                    {c.photo && (
                      <img
                        className="Main-Accepted-Quests-Quest-Photo-Img"
                        src={`${process.env.REACT_APP_BACKEND_HOST}/${c.photo}`}
                        alt=""
                      ></img>
                    )}
                  </div>
                </div>
              </div>
            );
          })}
        </Carousel>
      )}
      {/* <div className="FrontPage-Display-Box">
            {questBoardAcceptedQuest.length > 0 && (<div>
                <div className="FrontPage-Quest-Name">Accepted Quests</div>
                <div className="FrontPage-Quest-Display">
                    <div className="FrontPage-Quest">
                        {questBoardAcceptedQuest.length > 0 && (<div>
							<div className="FrontPage-Quest-Number">
								Quest: {questBoardAcceptedQuest.filter(quest=>quest.took_by_user === userId)
								.map(quest =>quest.quest_name)}
							</div>
                            <div className="FrontPage-Quest-Content">
								{questBoardAcceptedQuest.filter(quest=>quest.took_by_user === userId)
								.map(quest => quest.about_quest)}
							</div>
                        </div>)}
                        <div className="FrontPage-Quest-Icon">
                            <img src={`${process.env.REACT_APP_BACKEND_HOST}/${questBoardAcceptedQuest.filter(quest=>quest.took_by_user === userId).map(quest => quest.photo)}`} alt=""></img>
                        </div>
                    </div>
                    <div className="FrontPage-Prize-Display">
                        <div className="FrontPage-Prize">Prize: $ {questBoardAcceptedQuest.filter(quest=>quest.took_by_user === userId).map(quest => quest.prize)}</div>
                        <div className="FrontPage-Details-Button">
                            <div className="FrontPage-Details-Color" onClick = {()=>{
                                dispatch(push(`/questboard/${(questBoardAcceptedQuest.filter(quest=>quest.took_by_user === userId).map(quest => quest.quest_id))[0]}`))
                            }}>Details</div>
                        </div>
                    </div>
                    </div>
                </div>)}
            </div> */}
      {LoginUserPublishedQuest.length > 0 && (
        <Carousel>
          {LoginUserPublishedQuest.map((c, index) => {
            return (
              <div key={index} className="Main-Accepted-Quests-Display">
                <div className="Main-Accepted-Quests-Title">
                  Published Quests
                </div>
                <div className="Main-Accepted-Quests-Quest-Display">
                  <div className="Main-Accepted-Quests-Quest-Display-Box">
                    <div>
                      <div className="Main-Accepted-Quests-Star-Display">
                        <div className="Main-Accepted-Quests-Quest-Title">
                          {c.quest_name.substr(0, 10) + "..."}
                        </div>
                        <div className="Main-Accepted-Quests-Quest-Star">
                          {c.difficulty}
                        </div>
                      </div>
                      <div className="Main-Accepted-Quests-Quest-Content">
                        <div>{c.about_quest.substr(0, 20) + "..."}</div>
                      </div>
                    </div>
                    <div className="Main-Accepted-Quests-Quest-Details-Display">
                      <div className="Main-Accepted-Quests-Quest-Prize">
                        PRIZE: ${c.prize}
                      </div>
                      <div
                        className="Main-Accepted-Quests-Quest-Details"
                        onClick={() => {
                          dispatch(push("/questBoard/" + c.quest_id));
                        }}
                      >
                        Details
                      </div>
                    </div>
                  </div>
                  <div className="Main-Accepted-Quests-Quest-Photo">
                    {c.quest_photo && (
                      <img
                        className="Main-Accepted-Quests-Quest-Photo-Img"
                        src={`${process.env.REACT_APP_BACKEND_HOST}/${c.quest_photo}`}
                        alt=""
                      ></img>
                    )}
                  </div>
                </div>
              </div>
            );
          })}
        </Carousel>
      )}
      {/* <div className="FrontPage-Display-Box">
            {questBoardAcceptedQuest.length > 0 && (<div>
                <div className="FrontPage-Quest-Name">Published Quests</div>
                <div className="FrontPage-Quest-Display">
                    <div className="FrontPage-Quest">
						{questBoard.length > 0 && userId >= 1 && (<div>
                             <div className="FrontPage-Quest-Number">Quest: {questBoard.filter(quest=>quest.user_id === userId).map(quest => quest.quest_name)} </div>
                            <div className="FrontPage-Quest-Content">{questBoard.filter(quest=>quest.user_id === userId).map(quest => quest.about_quest)}</div>
                        </div>)}
                        <div className="FrontPage-Quest-Icon">
                            <img src={FrontPageIcon} alt=""></img>
                        </div>
                    </div>
                    <div className="FrontPage-Prize-Display">
                        {questBoard.length > 0 && <div className="FrontPage-Prize">Prize: $ {questBoard.filter(quest=>quest.user_id === userId).map(quest => quest.prize)}</div>}
                        <div className="FrontPage-Details-Button">
                            <div className="FrontPage-Details-Color" onClick = {()=>{
                                dispatch(push(`/questboard/${(questBoard.filter(quest=>quest.user_id === userId).map(quest => quest.quest_id))[0]}`))
                            }}>Details</div>
                        </div>
                    </div>
                </div>
                </div>)}
            </div> */}
      <div className="FrontPage-TakePublish-Button">
        <NavLink
          to="/questBoard"
          style={{ textDecoration: "none", color: "#4987C0" }}
        >
          <div
            className="FrontPage-Take-Button"
            onClick={() => {
              // props.setMain()
            }}
          >
            <img src={hand_up} alt=""></img>
            <div className="FrontPage-Button-Name">Take Quest</div>
          </div>
        </NavLink>
        <NavLink
          to="/publishQuest"
          style={{ textDecoration: "none", color: "#4987C0" }}
        >
          <div
            className="FrontPage-Publish-Button"
            onClick={() => {
              // props.setMain()
            }}
          >
            <img src={FrontPageIcon} alt=""></img>
            <div className="FrontPage-Button-Name">Publish Quest</div>
          </div>
        </NavLink>
      </div>
      <NavLink to="/main" style={{ textDecoration: "none", color: "#5CC7FC" }}>
        <div className="FrontPage-Menu">
          <div className="FrontPage-Menu-Name">
            <div className="FrontPage-Menu-Title">Go to Main Menu</div>
          </div>
        </div>
      </NavLink>
    </div>
  );
}
