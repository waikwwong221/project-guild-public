import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import './MyCard.css'
import gold_coin from './photo/gold_coin.png'
import { addCoins } from './redux/addConis/action';
import { fetchUserData } from './redux/UserData/action';
import { RootState } from './store';
import { confirmAlert } from 'react-confirm-alert'; // Import
import 'react-confirm-alert/src/react-confirm-alert.css'; // Import css
import { NavLink } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faArrowLeft } from '@fortawesome/free-solid-svg-icons'

export function MyCard() {
    const dispatch = useDispatch();
    useEffect(() => {
        dispatch(fetchUserData())
    }, [dispatch])
    const userData = useSelector((state: RootState) => state.userData.userData)
    const userId = useSelector((state: RootState) => state.auth.userId)
    const newUserData = userData.find(c => c.id === userId)

    const MyCardDate = [
        {
            COINS: 80,
            HKD: "HK$8.00",
        },
        {
            COINS: 180,
            HKD: "HK$15.00",
        },
        {
            COINS: 450,
            HKD: "HK$38.00",
        },
        {
            COINS: 900,
            HKD: "HK$78.00",
        },
        {
            COINS: 1650,
            HKD: "HK$158.00",
        },
        {
            COINS: 4500,
            HKD: "HK$398.00",
        },
        {
            COINS: 9400,
            HKD: "HK$788.00",
        },
    ]

    return (
        <div className="MyCard-Display">
            <div className="UserPage-BackMain">
                <NavLink to="/main" style={{textDecoration: 'none'}}>
                    <div className="UserPage-Main-Button">
                        <div className="UserPage-Main-Button-fa"><FontAwesomeIcon icon={faArrowLeft} /></div>
                        <div className="UserPage-Main-Button-Title">Main Menu</div>
                    </div>
                </NavLink>
            </div>
            <div className="MyCard-UserDate">
                <div className="MyCard-UserIcon-Display">
                    <img src={`${process.env.REACT_APP_BACKEND_HOST}/${newUserData?.photo}`} alt=""></img>
                </div>
                <div className="MyCard-UserName">{newUserData?.display_name}</div>
                <div className="MyCard-UserName-Money-Display">
                    <img className="MyCard-gold_coin" src={gold_coin} alt=""></img>
                    <div className="MyCard-UserName-Money">{newUserData?.currency}</div>
                </div>
            </div>
            <hr/>
            <div className="MyCard-Coins-Title">COINS</div>
            <hr/>
            <div className="MyCard-Coins-Display">
                {MyCardDate.map((c, index) => {
                    return(
                        <div key={index} className="MyCard-Coins-Date" onClick={() => {
                            confirmAlert({
                                title: 'Confirm to Deposit',
                                message: `Are you sure Deposit ${c.COINS} COINS?`,
                                buttons: [
                                  {
                                    label: 'Yes',
                                    onClick: () => {
                                        dispatch(addCoins(userId + "", c.COINS + ""))
                                        alert(`Deposit ${c.COINS} COINS!!`)
                                        dispatch(fetchUserData())
                                    }
                                  },
                                  {
                                    label: 'No',
                                    onClick: () => ""
                                  }
                                ]
                              });
                        }}>
                            <img src={gold_coin} alt=""></img>
                            <div className="MyCard-Coins-Money-Title">{c.COINS} COINS</div>
                            <div className="MyCard-Coins-Button">{c.HKD}</div>
                        </div>
                    )
                })}
            </div>
        </div>
    )
}